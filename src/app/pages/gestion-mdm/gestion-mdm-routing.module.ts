import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ParametrageComponent } from "./parametrage/parametrage.component";
import { DonneesComponent } from './donnees/donnees.component';

const routes: Routes = [
  {
    path: "",
    data: {
      title: "Gestion MDM",
    },
    children: [
      {
        path: "parametrage",
        component: ParametrageComponent,
        data: { title: "Paramétrage", idmodule: 3024, idrubrique: 5176 },
      },


      {
        path: "donnees",
        component: DonneesComponent,
        data: { title: "Donneés", idmodule: 3024, idrubrique: 5178 },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GestionMdmRoutingModule { }
