

export function getValidationFunction(ID_FORMAT_DONNEE: number): Function {
    switch (ID_FORMAT_DONNEE) {
        //?integer
        case 1:
            return (value: string): boolean => {
                // return !isNaN(+value);//float boolean ...
                return /^\d+$/.test(value);
            };

        default:
            return (value): boolean => {
                return false;
            };
    }
}
