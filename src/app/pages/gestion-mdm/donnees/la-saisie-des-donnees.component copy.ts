// import { AlertBoxService } from '../../../services/utils/alert-box.service';
// import { Component, OnInit, OnDestroy, ViewChildren, QueryList } from '@angular/core';
// import { FormArray, FormGroup, FormControl, Validators } from '@angular/forms';
// import { Model } from '../../../entities/gestion-mdm/MDM_MODEL';
// import { Canvas } from '../../../entities/gestion-mdm/MDM_CANVAS';
// import { ModelService } from '../../../services/gestion-mdm/Model/model.service';
// import { CanvasService } from '../../../services/gestion-mdm/Canvas/canvas.service';
// import { AuthService } from '../../../services/login/auth.service';
// import { Subject, forkJoin } from 'rxjs';
// import { takeUntil } from 'rxjs/operators';
// import { Param } from '../../../entities/gestion-mdm/MDM_PARAM';
// import { jqxGridComponent } from 'jqwidgets-ng/jqxgrid';
// import { JqxgridFrService } from '../../../services/utils/jqxgrid-fr.service';
// import { CanvasLigneService } from '../../../services/gestion-mdm/CanvasLigne/canvas-ligne.service';
// import { CanvasLigne } from '../../../entities/gestion-mdm/MDM_CANVAS_LIGNE';
// import { ParamLigneCanvas } from '../../../entities/gestion-mdm/MDM_PARAM_LIGNE_CANVAS';
// import { getValidationFunction } from '../MDM_CONSTANTS';
// import { ParamLigneCanvasService } from '../../../services/gestion-mdm/ParamLigneCanvas/param-ligne-canvas.service';
// import { ParamService } from '../../../services/gestion-mdm/Param/param.service';
// import Swal from 'sweetalert2';
// import { MotifService } from '../../../services/gestion-mdm/Motif/motif.service';
// import { Motif } from '../../../entities/gestion-mdm/MDM_MOTIF';

// @Component({
//   selector: 'app-la-saisie-des-donnees',
//   templateUrl: './la-saisie-des-donnees.component.html',
//   styleUrls: ['./la-saisie-des-donnees.component.css']
// })
// export class LaSaisieDesDonneesComponent implements OnInit, OnDestroy {
//   @ViewChildren(jqxGridComponent) CanvasGrids: QueryList<jqxGridComponent>;
//   public createpermission: boolean;
//   public editpermission: boolean;
//   public models: Model[];
//   public canvas: Canvas[];
//   public canvasLignes: CanvasLigne[] = [];
//   public canvasGridsSettings: Map<number, {
//     source: any, dataSource: any, localization: any, columns: any[]
//   }>;
//   public formGroup: FormGroup;
//   public showpopup = false;
//   public params: Param[];
//   private subscriptions: Subject<void> = new Subject<void>();
//   public modelsdropdownSettings: any = {
//     singleSelection: true,
//     idField: "ID_MDM_MODEL",
//     textField: "LIBELLE",
//   };
//   private selectedcelldata: any;
//   constructor(
//     private modelService: ModelService,
//     private paramService: ParamService,
//     private canvasService: CanvasService,
//     private canvasLigneService: CanvasLigneService,
//     private paramLigneCanvasService: ParamLigneCanvasService,
//     private alertBoxService: AlertBoxService,
//     public authService: AuthService,
//     private jqxgridFrService: JqxgridFrService,
//     private motifService: MotifService,

//   ) { }
//   ngOnDestroy(): void {

//   }

//   ngOnInit(): void {
//     this.createpermission = true;
//     this.editpermission = true;
//     this.initModelDropDown();
//     this.formGroup = new FormGroup(({
//       motif: new FormControl(null, [Validators.required, Validators.maxLength(150)])
//     }));
//   }

//   initModelDropDown() {
//     this.modelService
//       .findAllActif()
//       .pipe(takeUntil(this.subscriptions))
//       .subscribe(
//         (value) => {
//           this.models = value.data;
//         },
//         (error) => {
//           this.alertBoxService.alert({
//             icon: "error",
//             title: error.statusText,
//             text: error.error.message,
//           });
//         }
//       );
//   }
//   initCanvasList(model: Model) {
//     this.canvasService.findAllActifBelongsTo(model.ID_MDM_MODEL).pipe(takeUntil(this.subscriptions))
//       .subscribe(
//         (value) => {
//           this.canvas = value.data;
//           this.canvasGridsSettings = new Map();
//           this.canvas.forEach(element => {
//             this.initCanvasGrid(element);
//             this.fillCanvasGrid(element);
//           });
//         },
//         (error) => {
//           this.alertBoxService.alert({
//             icon: "error",
//             title: error.statusText,
//             text: error.error.message,
//           });
//         }
//       );
//   }

//   initCanvasGrid(canvas: Canvas) {
//     let localization = this.jqxgridFrService.getLocalization("fr");

//     let source: any = {
//       datatype: "array",
//       datafields: [
//         { name: "ID_CANVAS_LIGNE", type: "int" },
//         { name: "SYNCHRO", type: "boolean" },
//         { name: "ANNULER", type: "boolean" },
//       ],
//       localdata: [],
//     };
//     let columns: any[] = [
//       {
//         text: "SYNCHRO",
//         datafield: "SYNCHRO",
//         width: "150",
//         columntype: "checkbox",
//         filtertype: "checkedlist",
//         editable: this.editpermission,
//         cellsalign: "center",
//         align: "center",
//       },
//       {
//         text: "ANNULER",
//         datafield: "ANNULER",
//         width: "150",
//         columntype: "checkbox",
//         filtertype: "checkedlist",
//         editable: this.editpermission,
//         cellsalign: "center",
//         align: "center",
//       },
//     ];
//     if (canvas.PARAMS) {
//       canvas.PARAMS.forEach(
//         async param => {
//           source.datafields.push({ name: "contenuid_" + param.ID_PARAM, type: "string" });
//           if (param.ID_FORMAT_DONNEE) {
//             source.datafields.push({ name: "param_" + param.ID_PARAM, type: "string" });
//             columns.push({
//               text: param.LIBELLE_PARAM,
//               datafield: "param_" + param.ID_PARAM,
//               editable: this.editpermission,
//               cellsalign: "center",
//               align: "center",
//               filtertype: "input",
//             });
//           } else {
//             source.datafields.push({ name: "param_" + param.ID_PARAM, type: "string" });
//             columns.push({
//               text: param.LIBELLE_PARAM,
//               datafield: "param_" + param.ID_PARAM,
//               editable: this.editpermission,
//               cellsalign: "center",
//               align: "center",
//               filtertype: "input",
//               // columntype: 'template',
//               columntype: 'dropdownlist',
//               createeditor: async (row: number, cellvalue: any, editor: any, cellText: any, width: any, height: any) => {
//                 let paramsource = {
//                   datatype: "array",
//                   datafields: [
//                     { name: param.VALEUR_AFFICHER, type: "String" },
//                     { name: param.VALEUR_RETOUR, type: "String" },
//                   ],
//                   localdata: [
//                   ],
//                 };
//                 let dataformapi = await this.getParamEditorDataAdapter(param).toPromise();
//                 (<Array<any>>dataformapi.data).forEach(element => {
//                   let donne: any = {};
//                   donne[String(param.VALEUR_AFFICHER)] = element[String(param.VALEUR_AFFICHER)];
//                   donne[String(param.VALEUR_RETOUR)] = element[String(param.VALEUR_RETOUR)];
//                   paramsource.localdata.push(donne);
//                 });
//                 let paramdataAdapter = new jqx.dataAdapter(paramsource);
//                 editor.jqxDropDownList({
//                   checkboxes: false, source: paramdataAdapter, displayMember: param.VALEUR_AFFICHER, valueMember: param.VALEUR_RETOUR, width: width, height: height,
//                   selectionRenderer: () => {
//                     return '<span style="top: 4px; position: relative;">Choisir une Valuer:</span>';
//                   }
//                 });
//               }
//             });
//           }
//         }
//       );
//     }
//     let dataSource = new jqx.dataAdapter(source);
//     // console.log(columns);


//     this.canvasGridsSettings.set(canvas.ID_CANVAS, {
//       source, dataSource, localization, columns
//     });
//   }
//   fillCanvasGrid(canvas: Canvas) {
//     this.canvasLigneService
//       .findAllIncludeParamLigneCanvas(canvas.ID_CANVAS)
//       .pipe(takeUntil(this.subscriptions))
//       .subscribe(
//         (value: { statusCode: any, data: CanvasLigne[], message: any }) => {
//           if (value.data) {
//             this.canvasLignes = this.canvasLignes.concat(value.data);
//             // console.log(this.canvasLignes);
//             this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata = [];
//             value.data.forEach(canvasLigne => {
//               let row: { [k: string]: any } = {};
//               row.ID_CANVAS_LIGNE = canvasLigne.ID_CANVAS_LIGNE;
//               row.SYNCHRO = canvasLigne.SYNCHRO;
//               row.ANNULER = canvasLigne.ANNULER;
//               if (canvasLigne.PARAM_LIGNE_CANVAS)
//                 canvasLigne.PARAM_LIGNE_CANVAS.forEach(async (paramLigneCanvas: ParamLigneCanvas) => {
//                   row["contenuid_" + paramLigneCanvas.ID_PARAM] = paramLigneCanvas.ID_PARAM_LIGNE_CANVAS;

//                   if (paramLigneCanvas.PARAM.ID_FORMAT_DONNEE) {
//                     row["param_" + paramLigneCanvas.ID_PARAM] = paramLigneCanvas.CONTENU;
//                   }
//                   else {
//                     // let apidata = await this.getParamEditorDataAdapter(paramLigneCanvas.PARAM).pipe(takeUntil(this.subscriptions)).toPromise();
//                     // let paramvaleurfromquery = (<Array<any>>apidata.data).find(element => element[paramLigneCanvas.PARAM.VALEUR_RETOUR] == paramLigneCanvas.CONTENU);
//                     // if (paramvaleurfromquery) {
//                     // row["param_" + paramLigneCanvas.ID_PARAM] = paramvaleurfromquery[paramLigneCanvas.PARAM.VALEUR_AFFICHER];

//                     if (paramLigneCanvas.CONTENU)
//                       row["param_" + paramLigneCanvas.ID_PARAM] = await this.getvaleurAfficher(paramLigneCanvas.CONTENU, paramLigneCanvas.PARAM);
//                     else
//                       row["param_" + paramLigneCanvas.ID_PARAM] = "";
//                     this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].updatebounddata();
//                     // }
//                   }
//                 });
//               // console.log(JSON.stringify(row));
//               // console.log(row);
//               this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata.push(row);
//               // console.log(this.can vasGridsSettings.get(canvas.ID_CANVAS).dataSource);
//               this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].updatebounddata();
//             });
//           }
//         },
//         (error) => {
//           this.alertBoxService.alert({
//             icon: "error",
//             title: error.statusText,
//             text: error.error.message,
//           });
//         }
//       );
//   }


//   async gridOnCellEndEdit(event: any, canvas: Canvas) {
//     // console.log(event);
//     let validationparamcontenu = new ParamLigneCanvas();
//     if (event.args.value != event.args.oldvalue) {
//       let param_id: number;
//       if ((<string>event.args.datafield) === "ANNULER" || (<string>event.args.datafield) === "SYNCHRO") {
//         if ((<string>event.args.datafield) === "ANNULER") {
//           if (event.args.row.ID_CANVAS_LIGNE != null) {
//             let canvasLigne1 = this.canvasLignes.find(cl => cl.ID_CANVAS_LIGNE === event.args.row.ID_CANVAS_LIGNE);
//             let canvasLigne = new CanvasLigne();
//             canvasLigne.ID_CANVAS_LIGNE = canvasLigne1.ID_CANVAS_LIGNE;
//             canvasLigne.ANNULER = event.args.value;
//             // console.log(JSON.stringify(canvasLigne));
//             this.canvasLigneService.update(canvasLigne).pipe(takeUntil(this.subscriptions))
//               .subscribe(
//                 (value) => {
//                   this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex][event.args.datafield] = event.args.value;
//                   this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].updatebounddata();
//                   this.alertBoxService.alertEdit();
//                 },
//                 (error) => {
//                   this.alertBoxService.alert({
//                     icon: "error",
//                     title: error.statusText,
//                     text: error.error.message,
//                   });
//                 }
//               );
//           } else {
//             //? changing annuler but no values in the row so no need to create a canvas ligne
//           }
//         } else {
//           if (event.args.row.ID_CANVAS_LIGNE != null) {
//             let canvasLigne1 = this.canvasLignes.find(cl => cl.ID_CANVAS_LIGNE === event.args.row.ID_CANVAS_LIGNE);
//             let canvasLigne = new CanvasLigne();
//             canvasLigne.ID_CANVAS_LIGNE = canvasLigne1.ID_CANVAS_LIGNE;
//             canvasLigne.SYNCHRO = event.args.value;
//             if (canvasLigne.SYNCHRO) {
//               this.canvasLigneService.NonValidParams(canvasLigne.ID_CANVAS_LIGNE).pipe(takeUntil(this.subscriptions)).subscribe(
//                 (value) => {
//                   if (value.data.length == 0) {
//                     this.canvasLigneService.update(canvasLigne).pipe(takeUntil(this.subscriptions))
//                       .subscribe(
//                         (value) => {
//                           this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex][event.args.datafield] = event.args.value;
//                           this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].updatebounddata();
//                           this.alertBoxService.alertEdit();
//                         },
//                         (error) => {
//                           this.alertBoxService.alert({
//                             icon: "error",
//                             title: error.statusText,
//                             text: error.error.message,
//                           });
//                           this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].updatebounddata();
//                         }
//                       );
//                   } else {
//                     this.alertBoxService.alert({
//                       icon: "error",
//                       title: "Non Valid Param",
//                       text: "This Ligne Have a Non Valid Param",
//                     });
//                     this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].updatebounddata();
//                   }
//                 },
//                 (error) => {
//                   this.alertBoxService.alert({
//                     icon: "error",
//                     title: error.statusText,
//                     text: error.error.message,
//                   });
//                 }
//               );
//             } else {
//               this.canvasLigneService.update(canvasLigne).pipe(takeUntil(this.subscriptions))
//                 .subscribe(
//                   (value) => {
//                     this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex][event.args.datafield] = event.args.value;
//                     this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].updatebounddata();
//                     this.alertBoxService.alertEdit();
//                   },
//                   (error) => {
//                     this.alertBoxService.alert({
//                       icon: "error",
//                       title: error.statusText,
//                       text: error.error.message,
//                     });
//                   }
//                 );
//             }
//           }
//         }

//       } else {
//         param_id = +(<string>event.args.datafield).split("_")[1];

//         // let param_id: number = +(<string>event.args.datafield).split("_")[1];
//         let param: Param = canvas.PARAMS.find((p: Param) => p.ID_PARAM === param_id);
//         // console.log(event, canvas);
//         if (event.args.row.ID_CANVAS_LIGNE != null) {
//           if (param.ID_FORMAT_DONNEE)//? false for predefined params
//           {
//             let id_format_donnee = param.ID_FORMAT_DONNEE; let validate = getValidationFunction(id_format_donnee);
//             if (validate(event.args.value)) {
//               let paramLigneCanvas: ParamLigneCanvas = new ParamLigneCanvas();
//               paramLigneCanvas.ID_CANVAS_LIGNE = event.args.row.ID_CANVAS_LIGNE;
//               // console.log(this.canvasLignes);
//               // console.log(this.canvasLignes.find(cl => cl.ID_CANVAS_LIGNE === 1));
//               // console.log("lignes", this.canvasLignes);
//               // console.log("paramlc", paramLigneCanvas);
//               let canvasLigne = this.canvasLignes.find(cl => cl.ID_CANVAS_LIGNE === paramLigneCanvas.ID_CANVAS_LIGNE);
//               // console.log(canvasLigne);

//               let oldparamLigneCanvas = canvasLigne.PARAM_LIGNE_CANVAS.find(plc => plc.ID_PARAM === param.ID_PARAM);
//               paramLigneCanvas.ID_PARAM_LIGNE_CANVAS = oldparamLigneCanvas.ID_PARAM_LIGNE_CANVAS;
//               paramLigneCanvas.ID_PARAM = param.ID_PARAM;
//               paramLigneCanvas.CONTENU = event.args.value;
//               // console.log(JSON.stringify(paramLigneCanvas));

//               this.paramLigneCanvasService.update(paramLigneCanvas).pipe(takeUntil(this.subscriptions))
//                 .subscribe(
//                   (value) => {
//                     this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex][event.args.datafield] = event.args.value;
//                     this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].updatebounddata();
//                     this.updatevalidparamlignecanvas(paramLigneCanvas);
//                   },
//                   (error) => {
//                     this.alertBoxService.alert({
//                       icon: "error",
//                       title: error.statusText,
//                       text: error.error.message,
//                     });
//                   }
//                 );
//             } else {
//               // this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex][event.args.datafield] = event.args.oldvalue;
//               // this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].updatebounddata();
//               this.fillCanvasGrid(canvas);
//               this.alertBoxService.alert({
//                 icon: "error",
//                 title: "Donnee Non Valid",
//                 text: "Donnee Non Valid",
//               });
//             }
//           } else {
//             let paramLigneCanvas: ParamLigneCanvas = new ParamLigneCanvas();
//             paramLigneCanvas.ID_CANVAS_LIGNE = event.args.row.ID_CANVAS_LIGNE;
//             let canvasLigne = this.canvasLignes.find(cl => cl.ID_CANVAS_LIGNE === paramLigneCanvas.ID_CANVAS_LIGNE);
//             let oldparamLigneCanvas = canvasLigne.PARAM_LIGNE_CANVAS.find(plc => plc.ID_PARAM === param.ID_PARAM);
//             paramLigneCanvas.ID_PARAM_LIGNE_CANVAS = oldparamLigneCanvas.ID_PARAM_LIGNE_CANVAS;
//             paramLigneCanvas.ID_PARAM = param.ID_PARAM;
//             paramLigneCanvas.CONTENU = await this.getvaleurRouteur(event.args.value, param);
//             this.paramLigneCanvasService.update(paramLigneCanvas).pipe(takeUntil(this.subscriptions))
//               .subscribe(
//                 (value) => {
//                   this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex][event.args.datafield] = event.args.value;
//                   this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].updatebounddata();
//                   this.updatevalidparamlignecanvas(paramLigneCanvas);
//                 },
//                 (error) => {
//                   this.alertBoxService.alert({
//                     icon: "error",
//                     title: error.statusText,
//                     text: error.error.message,
//                   });
//                 }
//               );
//           }
//         } else {
//           //? inserting new Linge in db
//           if (param.ID_FORMAT_DONNEE)//? false for predefined params
//           {
//             let id_format_donnee = param.ID_FORMAT_DONNEE; let validate = getValidationFunction(id_format_donnee);
//             if (validate(event.args.value)) {
//               let paramLigneCanvas: ParamLigneCanvas = new ParamLigneCanvas();

//               let canvasLigne = new CanvasLigne();
//               canvasLigne.ANNULER = event.args.row.ANNULER;
//               this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex]["ANNULER"] = event.args.row.ANNULER;
//               paramLigneCanvas.ID_PARAM = param.ID_PARAM;
//               paramLigneCanvas.CONTENU = event.args.value;
//               canvasLigne.PARAM_LIGNE_CANVAS = [];
//               paramLigneCanvas.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
//               paramLigneCanvas.DATETIME_SAISIE = new Date();
//               canvasLigne.PARAM_LIGNE_CANVAS.push(paramLigneCanvas);
//               canvasLigne.ID_CANVAS = canvas.ID_CANVAS;
//               canvasLigne.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
//               canvasLigne.DATETIME_SAISIE = new Date();
//               canvas.PARAMS.forEach(p => {
//                 if (p.ID_PARAM != param.ID_PARAM) {
//                   let paramLigneCanvas: ParamLigneCanvas = new ParamLigneCanvas();
//                   paramLigneCanvas.ID_PARAM = p.ID_PARAM;
//                   paramLigneCanvas.CONTENU = null;
//                   paramLigneCanvas.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
//                   paramLigneCanvas.DATETIME_SAISIE = new Date();
//                   canvasLigne.PARAM_LIGNE_CANVAS.push(paramLigneCanvas);
//                 }
//               });

//               this.canvasLigneService.createWithParams(canvasLigne).pipe(takeUntil(this.subscriptions))
//                 .subscribe(
//                   (value) => {
//                     this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex]["ID_CANVAS_LIGNE"] = value.data.ID_CANVAS_LIGNE;
//                     this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex][event.args.datafield] = event.args.value;
//                     console.log(value);
//                     // let plc = new ParamLigneCanvas();
//                     let plc = (<Array<ParamLigneCanvas>>value.data.PARAM_LIGNE_CANVAS).find(p => p.ID_PARAM == paramLigneCanvas.ID_PARAM);
//                     this.updatevalidparamlignecanvas(plc);

//                     if (this.canvasLignes) {
//                       this.canvasLignes.push(value.data);
//                     }
//                     this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].updatebounddata();
//                   },
//                   (error) => {
//                     this.alertBoxService.alert({
//                       icon: "error",
//                       title: error.statusText,
//                       text: error.error.message,
//                     });
//                   }
//                 );

//             } else {
//               // this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex][event.args.datafield] = event.args.oldvalue;
//               // this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].updatebounddata();
//               this.fillCanvasGrid(canvas);
//               this.alertBoxService.alert({
//                 icon: "error",
//                 title: "Donnee Non Valid",
//                 text: "Donnee Non Valid",
//               });
//             }
//           } else {

//             let paramLigneCanvas: ParamLigneCanvas = new ParamLigneCanvas();
//             // paramLigneCanvas.ID_CANVAS_LIGNE = event.args.row.ID_CANVAS_LIGNE;
//             // console.log(this.canvasLignes);
//             // console.log(this.canvasLignes.find(cl => cl.ID_CANVAS_LIGNE === 1));

//             // let canvasLigne = this.canvasLignes.find(cl => cl.ID_CANVAS_LIGNE === paramLigneCanvas.ID_CANVAS_LIGNE);
//             // console.log(canvasLigne);

//             // let oldparamLigneCanvas = canvasLigne.PARAM_LIGNE_CANVAS.find(plc => plc.ID_PARAM === param.ID_PARAM);
//             // paramLigneCanvas.ID_PARAM_LIGNE_CANVAS = oldparamLigneCanvas.ID_PARAM_LIGNE_CANVAS;
//             let canvasLigne = new CanvasLigne();
//             canvasLigne.ANNULER = event.args.row.ANNULER;
//             this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex]["ANNULER"] = event.args.row.ANNULER;
//             paramLigneCanvas.ID_PARAM = param.ID_PARAM;
//             paramLigneCanvas.CONTENU = await this.getvaleurRouteur(event.args.value, param);
//             canvasLigne.PARAM_LIGNE_CANVAS = [];
//             paramLigneCanvas.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
//             paramLigneCanvas.DATETIME_SAISIE = new Date();
//             canvasLigne.PARAM_LIGNE_CANVAS.push(paramLigneCanvas);
//             canvasLigne.ID_CANVAS = canvas.ID_CANVAS;
//             canvasLigne.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
//             canvasLigne.DATETIME_SAISIE = new Date();
//             canvas.PARAMS.forEach(p => {
//               if (p.ID_PARAM != param.ID_PARAM) {
//                 let paramLigneCanvas: ParamLigneCanvas = new ParamLigneCanvas();
//                 paramLigneCanvas.ID_PARAM = p.ID_PARAM;
//                 paramLigneCanvas.CONTENU = null;
//                 paramLigneCanvas.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
//                 paramLigneCanvas.DATETIME_SAISIE = new Date();
//                 canvasLigne.PARAM_LIGNE_CANVAS.push(paramLigneCanvas);
//               }
//             });
//             this.canvasLigneService.createWithParams(canvasLigne).pipe(takeUntil(this.subscriptions))
//               .subscribe(
//                 (value) => {
//                   this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex]["ID_CANVAS_LIGNE"] = value.data.ID_CANVAS_LIGNE;
//                   this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex][event.args.datafield] = event.args.value;
//                   if (this.canvasLignes) {
//                     this.canvasLignes.push(value.data);
//                   }
//                   let plc = (<Array<ParamLigneCanvas>>value.data.PARAM_LIGNE_CANVAS).find(p => p.ID_PARAM == paramLigneCanvas.ID_PARAM);
//                   this.updatevalidparamlignecanvas(plc);

//                   this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].updatebounddata();
//                 },
//                 (error) => {
//                   this.alertBoxService.alert({
//                     icon: "error",
//                     title: error.statusText,
//                     text: error.error.message,
//                   });
//                 }
//               );
//           }

//         }

//       }
//     } else {

//       if ((<string>event.args.datafield) != "ANNULER") {
//         let param_id: number; param_id = +(<string>event.args.datafield).split("_")[1];
//         let canvasLigne = this.canvasLignes.find(cl => cl.ID_CANVAS_LIGNE === event.args.row.ID_CANVAS_LIGNE);
//         let paramLigneCanvas = canvasLigne.PARAM_LIGNE_CANVAS.find(plc => plc.ID_PARAM === param_id);
//         this.updatevalidparamlignecanvas(paramLigneCanvas);
//       }
//     }


//   }
//   updatevalidparamlignecanvas(paramlignecanvas: ParamLigneCanvas) {
//     this.confirmValid("Valid Param", "is this value valid").then((value) => {
//       paramlignecanvas.VALID = value;
//       this.paramLigneCanvasService.update(paramlignecanvas).pipe(takeUntil(this.subscriptions))
//         .subscribe(
//           (value) => {
//             this.alertBoxService.alertEdit();
//           },
//           (error) => {
//             this.alertBoxService.alert({
//               icon: "error",
//               title: error.statusText,
//               text: error.error.message,
//             });
//           }
//         );
//     }).catch((err) => {
//       this.alertBoxService.alert({
//         icon: "error",
//         title: "err",
//         text: err,
//       });
//     })
//   }
//   confirmValid(titre: string, question: string): Promise<boolean> {

//     const editedSwal = Swal.mixin({
//       customClass: {
//         confirmButton: 'btn btn-success',
//         cancelButton: 'ml-3 btn btn-danger'
//       },
//       buttonsStyling: false
//     });

//     return new Promise<any>((resolve, reject) => {

//       editedSwal.fire({
//         title: '<strong>' + titre + '</strong>',
//         icon: 'question',
//         text: question,
//         showCancelButton: true,
//         focusConfirm: false,
//         confirmButtonText:
//           '<i class="fa fa-check"></i> Oui',
//         cancelButtonText:
//           '<i class="fa fa-close"></i> Non'
//       })
//         .then((result) => {
//           if (result.value) {
//             resolve(true);
//           } else {
//             resolve(false);
//           }
//         })
//         .catch(e => reject(e));
//     });

//   }


//   gridPageChange(event: any, canvas: Canvas) {

//   }
//   getParamEditorDataAdapter(param: Param) {
//     return this.paramService.findQueryDonneeData(param.ID_PARAM).pipe(takeUntil(this.subscriptions));
//     // let paramdataAdapter = new jqx.dataAdapter(paramsource);
//     // // console.log(paramdataAdapter);
//     // return paramdataAdapter;
//   }
//   async getvaleurAfficher(valeurRouteur: any, param: Param) {
//     let value = await this.paramService.findQueryDonneeData(param.ID_PARAM).pipe(takeUntil(this.subscriptions)).toPromise();
//     let vAfficher: string = null;
//     if (value.data) {
//       let paramvaleurfromquery = (<Array<any>>value.data).find(element => element[param.VALEUR_RETOUR] == valeurRouteur);
//       if (paramvaleurfromquery) {
//         vAfficher = paramvaleurfromquery[param.VALEUR_AFFICHER];
//       }
//     }
//     if (!vAfficher)
//       this.alertBoxService.alert({
//         icon: "error",
//         title: "Query Donne not found",
//         text: "",
//       });
//     // console.log(vAfficher, param);
//     // console.log(vAfficher);

//     return vAfficher;
//   }
//   async getvaleurRouteur(valeurAfficher: any, param: Param) {
//     let value = await this.paramService.findQueryDonneeData(param.ID_PARAM).pipe(takeUntil(this.subscriptions)).toPromise();
//     let vRetour: string = null;
//     if (value.data) {
//       let paramvaleurfromquery = (<Array<any>>value.data).find(element => element[param.VALEUR_AFFICHER] == valeurAfficher);
//       if (paramvaleurfromquery) {
//         vRetour = paramvaleurfromquery[param.VALEUR_RETOUR];
//       }
//     }
//     if (!vRetour) {
//       this.alertBoxService.alert({
//         icon: "error",
//         title: "Query Donne not found",
//         text: "",
//       });
//     }
//     return vRetour;
//   }

//   addLigne(canvas: Canvas) {
//     // console.log(canvas);
//     if (!this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[0] || this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[0].ID_CANVAS_LIGNE != null) {
//       this.canvasGridsSettings.get(canvas.ID_CANVAS).dataSource;
//       let row: any = {};
//       row.ID_CANVAS_LIGNE = null;
//       row.SYNCHRO = false;
//       row.ANNULER = false;
//       canvas.PARAMS.forEach(param => {
//         row["param_" + param.ID_PARAM] = null;
//       });
//       // this.canvasGridsSettings.get(canvas.ID_CANVAS).dataSource.cachedrecords.push(row);
//       this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata = [row].concat(this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata);
//       this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].updatebounddata();
//     } else {
//       this.alertBoxService.alert({
//         text: 'Veuilliez s\'il vous plait remplir tous les champs',
//         title: 'Ajout de Canvas Linge',
//         icon: 'error'
//       });
//     }
//   }

//   rejectSelectedCell(canvas: Canvas) {
//     console.log(
//       this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].getselectedcell());
//     if (
//       this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].getselectedcell() && (<string>this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].getselectedcell().datafield).includes('param')) {
//       let paramName = canvas.PARAMS.find(p => p.ID_PARAM == +(<string>this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].getselectedcell().datafield).split("_")[1]).LIBELLE_PARAM

//       this.selectedcelldata = {
//         cell: this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].getselectedcell(), canvas, paramName
//       }
//       this.showpopup = true;

//     } else {
//       this.alertBoxService.alert({
//         icon: "error",
//         title: "NO cell is selected",
//         text: "select a Param cell to reject",
//       });
//     }

//   }


//   addMotif() {
//     if (this.formGroup.valid) {
//       // console.log(this.dbclickevent);
//       let param_id = +(<string>this.selectedcelldata.cell.datafield).split("_")[1];
//       let contenuid = this.canvasGridsSettings.get(this.selectedcelldata.canvas.ID_CANVAS).source.localdata[this.selectedcelldata.cell.rowindex]["contenuid_" + param_id];
//       // let contenuid = this.dbclickevent.event.args.row.bounddata["contenuid_" + param_id];
//       // console.log(contenuid);
//       let motif = new Motif();
//       motif.ID_PARAM_CONTENU = contenuid;
//       motif.MOTIF = this.formGroup.get('motif').value;
//       motif.ID_OP_VERIFIER = this.authService.getDecodedToken().ID_UTILISATEUR;
//       motif.DATETIME_VERIFIER = new Date();
//       let paramLigneCanvas = new ParamLigneCanvas();
//       paramLigneCanvas.ID_PARAM_LIGNE_CANVAS = contenuid;
//       paramLigneCanvas.VALID = false;
//       paramLigneCanvas.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
//       paramLigneCanvas.DATETIME_SAISIE = new Date();
//       let canvasLigne = new CanvasLigne();
//       canvasLigne.ID_CANVAS_LIGNE = this.canvasGridsSettings.get(this.selectedcelldata.canvas.ID_CANVAS).source.localdata[this.selectedcelldata.cell.rowindex].ID_CANVAS_LIGNE;
//       canvasLigne.SYNCHRO = false;
//       canvasLigne.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
//       canvasLigne.DATETIME_SAISIE = new Date();
//       console.log(motif);
//       forkJoin([this.motifService.create(motif), this.paramLigneCanvasService.update(paramLigneCanvas), this.canvasLigneService.update(canvasLigne)]).pipe(takeUntil(this.subscriptions)).subscribe(
//         (value) => {
//           this.showpopup = false;
//           this.alertBoxService.alertEdit();
//           this.formGroup.reset();
//         },
//         (error) => {
//           this.alertBoxService.alert({
//             icon: "error",
//             title: error.statusText,
//             text: error.error.message,
//           });
//         }
//       );
//     }
//   }

//   refreshCanvasGrif(canvas: Canvas) {
//     this.fillCanvasGrid(canvas);
//   }
// }
