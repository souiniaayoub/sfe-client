import { AlertBoxService } from '../../../services/utils/alert-box.service';
import { Component, OnInit, OnDestroy, ViewChildren, QueryList, ViewChild, ElementRef, SystemJsNgModuleLoader } from '@angular/core';
import { FormArray, FormGroup, FormControl, Validators } from '@angular/forms';
import { Model } from '../../../entities/gestion-mdm/MDM_MODEL';
import { Canvas } from '../../../entities/gestion-mdm/MDM_CANVAS';
import { ModelService } from '../../../services/gestion-mdm/Model/model.service';
import { CanvasService } from '../../../services/gestion-mdm/Canvas/canvas.service';
import { AuthService } from '../../../services/login/auth.service';
import { Subject, forkJoin } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Param } from '../../../entities/gestion-mdm/MDM_PARAM';
import { jqxGridComponent } from 'jqwidgets-ng/jqxgrid';
import { JqxgridFrService } from '../../../services/utils/jqxgrid-fr.service';
import { CanvasLigneService } from '../../../services/gestion-mdm/CanvasLigne/canvas-ligne.service';
import { CanvasLigne } from '../../../entities/gestion-mdm/MDM_CANVAS_LIGNE';
import { ParamLigneCanvas } from '../../../entities/gestion-mdm/MDM_PARAM_LIGNE_CANVAS';
import { getValidationFunction } from '../MDM_CONSTANTS';
import { ParamLigneCanvasService } from '../../../services/gestion-mdm/ParamLigneCanvas/param-ligne-canvas.service';
import { ParamService } from '../../../services/gestion-mdm/Param/param.service';
import Swal from 'sweetalert2';
import { MotifService } from '../../../services/gestion-mdm/Motif/motif.service';
import { Motif } from '../../../entities/gestion-mdm/MDM_MOTIF';
import { Excel } from '../../../services/utils/excel';
import * as XLSX from 'xlsx';
import { OwlCalendarBodyComponent } from 'ng-pick-datetime/date-time/calendar-body.component';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-donnees',
  templateUrl: './donnees.component.html',
  styleUrls: ['./donnees.component.css']
})
export class DonneesComponent implements OnInit, OnDestroy {
  @ViewChildren(jqxGridComponent) CanvasGrids: QueryList<jqxGridComponent>;
  @ViewChild('fileInput') fileInput: ElementRef;
  public createpermission: boolean;
  public editpermission: boolean;
  public models: Model[];
  public aModelisSelected: boolean = false;
  public canvas: Canvas[];
  public canvastoinsertlines: Canvas;
  public errorsinfile: [{ ligne: number, param?: string, msg?: string }?] = [];
  public searshvalue: string = "";
  private QueryDonneeDataCach: Map<number, Array<any>> = new Map<number, Array<any>>();
  public canvasGridsSettings: Map<number, {
    source: any, dataSource: any, localization: any, columns: any[]
  }>;
  public formGroup: FormGroup;
  public excelformGroup: FormGroup;
  public showpopup = false;
  public showmultiplelinespopup = false;
  public params: Param[];
  private subscriptions: Subject<void> = new Subject<void>();
  public modelsdropdownSettings: any = {
    singleSelection: true,
    idField: "ID_MDM_MODEL",
    textField: "LIBELLE",
  };
  private selectedcelldata: any;
  constructor(
    private modelService: ModelService,
    private paramService: ParamService,
    private canvasService: CanvasService,
    private canvasLigneService: CanvasLigneService,
    private paramLigneCanvasService: ParamLigneCanvasService,
    private alertBoxService: AlertBoxService,
    public authService: AuthService,
    private jqxgridFrService: JqxgridFrService,
    private motifService: MotifService,
    private excel: Excel,
    private spinner: NgxSpinnerService,
  ) { }
  ngOnDestroy(): void {

  }

  ngOnInit(): void {
    this.createpermission = true;
    this.editpermission = true;
    this.initModelDropDown();
    this.formGroup = new FormGroup(({
      motif: new FormControl(null, [Validators.required, Validators.maxLength(150)])
    })); this.excelformGroup = new FormGroup(({
      file: new FormControl(null, [Validators.required])
    }));
  }
  changeSearchValue(event: any) {
    this.searshvalue = event.target.value;
  }

  async searchForCanvaLignes() {
    if (this.searshvalue && this.searshvalue != "" && this.searshvalue.replace(/\s/g, '').length != 0) {
      let search = this.searshvalue;
      // if (this.canvas[0] && this.canvas[0].PARAMS[0] && this.canvas[0].PARAMS.find(p => p.IS_PRIMARY_KEY = true).ID_FORMAT_DONNEE == null) {
      //   let p = this.canvas.find(c => c.PARAMS.find(pram => pram.IS_PRIMARY_KEY != true && pram.ID_FORMAT_DONNEE != null) || c.ID_CANVAS != this.canvas[0].ID_CANVAS);
      //   if (!p) {
      //     search = await this.getvaleurRouteur(this.searshvalue, this.canvas[0].PARAMS[0]);

      //   } else {
      //     this.alertBoxService.alert({
      //       icon: "error",
      //       title: "Problem dans parametrage de model",
      //       text: "this model has canvas with a primary key with format de donne and another canvas has primary key with valeur predifini",
      //     });
      //     return;
      //   }
      // }
      this.canvasLigneService.findAllByParamPK(search).pipe(takeUntil(this.subscriptions))
        .subscribe(
          (value) => {
            let PKs_PLC_has_Contenu: Array<ParamLigneCanvas> = value.data;
            this.canvas.forEach(c => {
              this.canvasGridsSettings.get(c.ID_CANVAS).source.localdata = [];
              this.CanvasGrids.toArray()[this.canvas.findIndex(cc => cc.ID_CANVAS == c.ID_CANVAS)].updatebounddata();
            });
            PKs_PLC_has_Contenu.forEach(pk => {
              let row: { [k: string]: any } = {};
              row.ID_CANVAS_LIGNE = pk.CANVAS_LIGNE.ID_CANVAS_LIGNE;
              row.SYNCHRO = pk.CANVAS_LIGNE.SYNCHRO;
              row.ANNULER = pk.CANVAS_LIGNE.ANNULER;
              pk.CANVAS_LIGNE.PARAM_LIGNE_CANVAS.forEach(async plc => {
                row["contenuid_" + plc.ID_PARAM] = plc.ID_PARAM_LIGNE_CANVAS;
                row["valid_" + plc.ID_PARAM] = plc.VALID;
                if (plc.PARAM.ID_FORMAT_DONNEE) {
                  row["param_" + plc.ID_PARAM] = plc.CONTENU;
                }
                else {
                  if (plc.CONTENU)
                    row["param_" + plc.ID_PARAM] = await this.getvaleurAfficher(plc.CONTENU, plc.PARAM);
                  else
                    row["param_" + plc.ID_PARAM] = "";
                  this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == pk.CANVAS_LIGNE.ID_CANVAS)].updatebounddata();
                }
              });
              this.canvasGridsSettings.get(pk.CANVAS_LIGNE.ID_CANVAS).source.localdata.push(row);
              this.initCanvasGridWithRenderer(pk.CANVAS_LIGNE.CANVAS);
              this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == pk.CANVAS_LIGNE.ID_CANVAS)].updatebounddata();
            });
          },
          (error) => {
            this.alertBoxService.alert({
              icon: "error",
              title: error.statusText,
              text: error.error.message,
            });
          }
        );
    } else {
      this.alertBoxService.alert({
        icon: "error",
        title: "tu n'as rien cherché ",
        text: "le champ de recherche est vide ",
      });
    }

  }
  initModelDropDown() {
    this.modelService
      .findAllActif()
      .pipe(takeUntil(this.subscriptions))
      .subscribe(
        (value) => {
          this.models = value.data;
        },
        (error) => {
          this.alertBoxService.alert({
            icon: "error",
            title: error.statusText,
            text: error.error.message,
          });
        }
      );
  }
  initCanvasList(model: Model) {

    this.aModelisSelected = true;
    this.canvasService.findAllActifBelongsTo(model.ID_MDM_MODEL).pipe(takeUntil(this.subscriptions))
      .subscribe(
        (value) => {
          this.canvas = value.data;
          this.canvasGridsSettings = new Map();
          this.canvas.forEach(element => {
            this.initCanvasGrid(element);
            //? after end of fill
            // this.initCanvasGridWithRenderer(element);

            this.fillCanvasGrid(element);
          });
        },
        (error) => {
          this.alertBoxService.alert({
            icon: "error",
            title: error.statusText,
            text: error.error.message,
          });
        }
      );
  }

  initCanvasGrid(canvas: Canvas) {
    this.spinner.show();
    let localization = this.jqxgridFrService.getLocalization("fr");

    let source: any = {
      datatype: "array",
      datafields: [
        { name: "ID_CANVAS_LIGNE", type: "int" },
        { name: "SYNCHRO", type: "boolean" },
        { name: "ANNULER", type: "boolean" },
      ],
      localdata: [],
    };
    let columns: any[] = [
      {
        text: "ID Canvas Ligne",
        datafield: "ID_CANVAS_LIGNE",
        width: "150",
        editable: false,
        cellsalign: "center",
        align: "center",
      }, {
        text: "synchronisée",
        datafield: "SYNCHRO",
        width: "150",
        columntype: "checkbox",
        filtertype: "checkedlist",
        editable: false,
        cellsalign: "center",
        align: "center",
      },
      {
        text: "annuler",
        datafield: "ANNULER",
        width: "150",
        columntype: "checkbox",
        filtertype: "checkedlist",
        editable: this.editpermission,
        cellsalign: "center",
        align: "center",
      },
    ];
    if (canvas.PARAMS) {
      canvas.PARAMS.forEach(
        async param => {
          source.datafields.push({ name: "contenuid_" + param.ID_PARAM, type: "string" });
          source.datafields.push({ name: "valid_" + param.ID_PARAM, type: "boolean" });
          if (param.ID_FORMAT_DONNEE) {
            source.datafields.push({ name: "param_" + param.ID_PARAM, type: "string" });
            columns.push({
              text: param.LIBELLE_PARAM,
              datafield: "param_" + param.ID_PARAM,
              editable: this.editpermission,
              cellsalign: "center",
              align: "center",
              filtertype: "input"
            });
          } else {
            source.datafields.push({ name: "param_" + param.ID_PARAM, type: "string" });
            columns.push({
              text: param.LIBELLE_PARAM,
              datafield: "param_" + param.ID_PARAM,
              editable: this.editpermission,
              cellsalign: "center",
              align: "center",
              filtertype: "input",
              // columntype: 'template',
              columntype: 'dropdownlist',
              createeditor: async (row: number, cellvalue: any, editor: any, cellText: any, width: any, height: any) => {
                let paramsource = {
                  datatype: "array",
                  datafields: [
                    { name: param.VALEUR_AFFICHER, type: "String" },
                    { name: param.VALEUR_RETOUR, type: "String" },
                  ],
                  localdata: [
                  ],
                };
                let dataformapi = await this.getParamEditorDataAdapter(param).toPromise();
                (<Array<any>>dataformapi.data).forEach(element => {
                  let donne: any = {};
                  donne[String(param.VALEUR_AFFICHER)] = element[String(param.VALEUR_AFFICHER)];
                  donne[String(param.VALEUR_RETOUR)] = element[String(param.VALEUR_RETOUR)];
                  paramsource.localdata.push(donne);
                });
                let paramdataAdapter = new jqx.dataAdapter(paramsource);
                editor.jqxDropDownList({
                  checkboxes: false, source: paramdataAdapter, displayMember: param.VALEUR_AFFICHER, valueMember: param.VALEUR_RETOUR, width: width, height: height,
                  selectionRenderer: () => {
                    return '<span style="top: 4px;  position: relative;">Choisir une Valuer:</span>';
                  }
                });
              }
            });
          }
        }
      );
    }
    let dataSource = new jqx.dataAdapter(source);
    this.canvasGridsSettings.set(canvas.ID_CANVAS, {
      source, dataSource, localization, columns
    }); this.spinner.hide();
  }
  initCanvasGridWithRenderer(canvas: Canvas) {
    this.spinner.show();

    let columns: any[] = [
      {
        text: "ID Canvas Ligne",
        datafield: "ID_CANVAS_LIGNE",
        width: "150",
        editable: false,
        cellsalign: "center",
        align: "center",
      }, {
        text: "synchronisée",
        datafield: "SYNCHRO",
        width: "150",
        columntype: "checkbox",
        filtertype: "checkedlist",
        editable: false,
        cellsalign: "center",
        align: "center",
      },
      {
        text: "annuler",
        datafield: "ANNULER",
        width: "150",
        columntype: "checkbox",
        filtertype: "checkedlist",
        editable: this.editpermission,
        cellsalign: "center",
        align: "center",
      },
    ];
    if (canvas.PARAMS) {
      canvas.PARAMS.forEach(
        async param => {
          if (param.ID_FORMAT_DONNEE) {
            columns.push({
              text: param.LIBELLE_PARAM,
              datafield: "param_" + param.ID_PARAM,
              editable: this.editpermission,
              cellsalign: "center",
              align: "center",
              filtertype: "input", cellsrenderer: this.getRenderer(canvas),
            });
          } else {
            columns.push({
              text: param.LIBELLE_PARAM,
              datafield: "param_" + param.ID_PARAM,
              editable: this.editpermission,
              cellsalign: "center",
              align: "center",
              filtertype: "input",
              columntype: 'dropdownlist', cellsrenderer: this.getRenderer(canvas),
              createeditor: async (row: number, cellvalue: any, editor: any, cellText: any, width: any, height: any) => {
                let paramsource = {
                  datatype: "array",
                  datafields: [
                    { name: param.VALEUR_AFFICHER, type: "String" },
                    { name: param.VALEUR_RETOUR, type: "String" },
                  ],
                  localdata: [
                  ],
                };
                let dataformapi = await this.getParamEditorDataAdapter(param).toPromise();
                (<Array<any>>dataformapi.data).forEach(element => {
                  let donne: any = {};
                  donne[String(param.VALEUR_AFFICHER)] = element[String(param.VALEUR_AFFICHER)];
                  donne[String(param.VALEUR_RETOUR)] = element[String(param.VALEUR_RETOUR)];
                  paramsource.localdata.push(donne);
                });
                let paramdataAdapter = new jqx.dataAdapter(paramsource);
                editor.jqxDropDownList({
                  checkboxes: false, source: paramdataAdapter, displayMember: param.VALEUR_AFFICHER, valueMember: param.VALEUR_RETOUR, width: width, height: height,
                  selectionRenderer: () => {
                    return '<span style="top: 4px;  position: relative;">Choisir une Valuer:</span>';
                  }
                });
              }
            });
          }
        }
      );
    }

    this.canvasGridsSettings.get(canvas.ID_CANVAS).columns = columns;
    this.spinner.hide();

  }
  getRenderer(canvas: Canvas) {

    let datasource = this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata;


    let renderer = (row: number, column: any, value: string): string => {
      return (datasource[row] && datasource[row]['valid_' + (<string>column).split('_')[1]]) ? '<span class="text-success " style= " width:100%; display:block;text-align:center; cursor:default;   margin-top: 9px; float:left">' + value + '</span>' : '<span class="text-danger " style= " width:100%; display:block;text-align:center; cursor:default;   margin-top: 9px; float:left">' + value + '</span>';
    }
    return renderer;
  }

  fillCanvasGrid(canvas: Canvas) {
    this.spinner.show();
    this.canvasLigneService
      .findAllIncludeParamLigneCanvas(canvas.ID_CANVAS)
      .pipe(takeUntil(this.subscriptions))
      .subscribe(
        async (value: { statusCode: any, data: CanvasLigne[], message: any }) => {
          if (value.data) {
            this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata = [];
            for (let index = 0; index < value.data.length; index++) {
              const canvasLigne = value.data[index];
              let row: { [k: string]: any } = {};
              row.ID_CANVAS_LIGNE = canvasLigne.ID_CANVAS_LIGNE;
              row.SYNCHRO = canvasLigne.SYNCHRO;
              row.ANNULER = canvasLigne.ANNULER;
              if (canvasLigne.PARAM_LIGNE_CANVAS)
                for (let index = 0; index < canvasLigne.PARAM_LIGNE_CANVAS.length; index++) {
                  const paramLigneCanvas = canvasLigne.PARAM_LIGNE_CANVAS[index];
                  row["contenuid_" + paramLigneCanvas.ID_PARAM] = paramLigneCanvas.ID_PARAM_LIGNE_CANVAS;
                  row["valid_" + paramLigneCanvas.ID_PARAM] = paramLigneCanvas.VALID;

                  if (paramLigneCanvas.PARAM.ID_FORMAT_DONNEE) {
                    row["param_" + paramLigneCanvas.ID_PARAM] = paramLigneCanvas.CONTENU;
                  }
                  else {
                    if (paramLigneCanvas.CONTENU)
                      row["param_" + paramLigneCanvas.ID_PARAM] = await this.getvaleurAfficher(paramLigneCanvas.CONTENU, paramLigneCanvas.PARAM);
                    else
                      row["param_" + paramLigneCanvas.ID_PARAM] = "";
                    this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].updatebounddata();
                  }

                }
              this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata.push(row);
              this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].updatebounddata();

            }
          }


          //? after end of fill 
          this.spinner.hide();
          this.initCanvasGridWithRenderer(canvas);
        },
        (error) => {
          this.alertBoxService.alert({
            icon: "error",
            title: error.statusText,
            text: error.error.message,
          });
        }
      );
  }
  async gridOnCellEndEdit(event: any, canvas: Canvas) {
    if (event.args.value != event.args.oldvalue) {
      let param_id: number;
      if ((<string>event.args.datafield) === "ANNULER" || (<string>event.args.datafield) === "SYNCHRO") {
        if ((<string>event.args.datafield) === "ANNULER") {
          if (event.args.row.ID_CANVAS_LIGNE != null) {
            let canvasLigne = new CanvasLigne();
            canvasLigne.ID_CANVAS_LIGNE = event.args.row.ID_CANVAS_LIGNE;
            canvasLigne.ANNULER = event.args.value;
            canvasLigne.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
            canvasLigne.DATETIME_SAISIE = new Date();
            this.canvasLigneService.update(canvasLigne).pipe(takeUntil(this.subscriptions))
              .subscribe(
                (value) => {
                  this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex][event.args.datafield] = event.args.value;
                  this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].updatebounddata();
                  this.alertBoxService.alertEdit();
                },
                (error) => {
                  this.alertBoxService.alert({
                    icon: "error",
                    title: error.statusText,
                    text: error.error.message,
                  });
                }
              );
          }
        } else {
          if (event.args.row.ID_CANVAS_LIGNE != null) {
            let canvasLigne = new CanvasLigne();
            canvasLigne.ID_CANVAS_LIGNE = event.args.row.ID_CANVAS_LIGNE;
            canvasLigne.SYNCHRO = event.args.value;
            canvasLigne.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
            canvasLigne.DATETIME_SAISIE = new Date();

            if (canvasLigne.SYNCHRO) {
              this.canvasLigneService.NonValidParams(canvasLigne.ID_CANVAS_LIGNE).pipe(takeUntil(this.subscriptions)).subscribe(
                (value) => {
                  if (value.data.length == 0) {
                    this.canvasLigneService.update(canvasLigne).pipe(takeUntil(this.subscriptions))
                      .subscribe(
                        (value) => {
                          this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex][event.args.datafield] = event.args.value;
                          this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].updatebounddata();
                          this.alertBoxService.alertEdit();
                        },
                        (error) => {
                          this.alertBoxService.alert({
                            icon: "error",
                            title: error.statusText,
                            text: error.error.message,
                          });
                          this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].updatebounddata();
                        }
                      );
                  } else {
                    this.alertBoxService.alert({
                      icon: "error",
                      title: "Paramètre invalide",
                      text: "Cette ligne a un paramètre non valide",
                    });
                    this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].updatebounddata();
                  }
                },
                (error) => {
                  this.alertBoxService.alert({
                    icon: "error",
                    title: error.statusText,
                    text: error.error.message,
                  });
                }
              );
            } else {
              this.canvasLigneService.update(canvasLigne).pipe(takeUntil(this.subscriptions))
                .subscribe(
                  (value) => {
                    this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex][event.args.datafield] = event.args.value;
                    this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].updatebounddata();
                    this.alertBoxService.alertEdit();
                  },
                  (error) => {
                    this.alertBoxService.alert({
                      icon: "error",
                      title: error.statusText,
                      text: error.error.message,
                    });
                  }
                );
            }
          }
        }
      } else {
        param_id = +(<string>event.args.datafield).split("_")[1];
        let param: Param = canvas.PARAMS.find((p: Param) => p.ID_PARAM === param_id);
        if (event.args.row.ID_CANVAS_LIGNE != null) {
          if (param.ID_FORMAT_DONNEE)//? false for predefined params
          {
            let id_format_donnee = param.ID_FORMAT_DONNEE; let validate = getValidationFunction(id_format_donnee);
            if (validate(event.args.value)) {
              let paramLigneCanvas: ParamLigneCanvas = new ParamLigneCanvas();
              paramLigneCanvas.ID_CANVAS_LIGNE = event.args.row.ID_CANVAS_LIGNE;
              paramLigneCanvas.ID_PARAM_LIGNE_CANVAS = event.args.row["contenuid_" + param.ID_PARAM];
              paramLigneCanvas.ID_PARAM = param.ID_PARAM;
              paramLigneCanvas.CONTENU = event.args.value;
              paramLigneCanvas.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
              paramLigneCanvas.DATETIME_SAISIE = new Date();

              this.paramLigneCanvasService.update(paramLigneCanvas).pipe(takeUntil(this.subscriptions))
                .subscribe(
                  (value) => {
                    this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex][event.args.datafield] = event.args.value;
                    this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].updatebounddata();
                    this.updatevalidparamlignecanvas(paramLigneCanvas, canvas, event);
                  },
                  (error) => {
                    this.alertBoxService.alert({
                      icon: "error",
                      title: error.statusText,
                      text: error.error.message,
                    });
                  }
                );
            } else {
              // this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex][event.args.datafield] = event.args.oldvalue;
              // this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].updatebounddata();
              this.fillCanvasGrid(canvas);
              this.alertBoxService.alert({
                icon: "error",
                title: "Données non valides",
                // text: "Données non valides",
              });
            }
          } else {
            let paramLigneCanvas: ParamLigneCanvas = new ParamLigneCanvas();
            paramLigneCanvas.ID_CANVAS_LIGNE = event.args.row.ID_CANVAS_LIGNE;
            paramLigneCanvas.ID_PARAM_LIGNE_CANVAS = event.args.row["contenuid_" + param.ID_PARAM];
            paramLigneCanvas.ID_PARAM = param.ID_PARAM;
            paramLigneCanvas.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
            paramLigneCanvas.DATETIME_SAISIE = new Date();

            paramLigneCanvas.CONTENU = await this.getvaleurRouteur(event.args.value, param);
            this.paramLigneCanvasService.update(paramLigneCanvas).pipe(takeUntil(this.subscriptions))
              .subscribe(
                (value) => {
                  this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex][event.args.datafield] = event.args.value;
                  this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].updatebounddata();
                  this.updatevalidparamlignecanvas(paramLigneCanvas, canvas, event);
                },
                (error) => {
                  this.alertBoxService.alert({
                    icon: "error",
                    title: error.statusText,
                    text: error.error.message,
                  });
                }
              );
          }
        } else {
          //? inserting new Linge in db
          if (param.ID_FORMAT_DONNEE)//? false for predefined params
          {
            let id_format_donnee = param.ID_FORMAT_DONNEE; let validate = getValidationFunction(id_format_donnee);
            if (validate(event.args.value)) {
              let paramLigneCanvas: ParamLigneCanvas = new ParamLigneCanvas();
              let canvasLigne = new CanvasLigne();
              canvasLigne.ANNULER = event.args.row.ANNULER;
              this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex]["ANNULER"] = event.args.row.ANNULER;
              paramLigneCanvas.ID_PARAM = param.ID_PARAM;
              paramLigneCanvas.CONTENU = event.args.value;
              canvasLigne.PARAM_LIGNE_CANVAS = [];
              paramLigneCanvas.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
              paramLigneCanvas.DATETIME_SAISIE = new Date();
              canvasLigne.PARAM_LIGNE_CANVAS.push(paramLigneCanvas);
              canvasLigne.ID_CANVAS = canvas.ID_CANVAS;
              canvasLigne.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
              canvasLigne.DATETIME_SAISIE = new Date();
              canvas.PARAMS.forEach(p => {
                if (p.ID_PARAM != param.ID_PARAM) {
                  let paramLigneCanvas: ParamLigneCanvas = new ParamLigneCanvas();
                  paramLigneCanvas.ID_PARAM = p.ID_PARAM;
                  paramLigneCanvas.CONTENU = null;
                  paramLigneCanvas.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
                  paramLigneCanvas.DATETIME_SAISIE = new Date();
                  canvasLigne.PARAM_LIGNE_CANVAS.push(paramLigneCanvas);
                }
              });
              this.canvasLigneService.createWithParams(canvasLigne).pipe(takeUntil(this.subscriptions))
                .subscribe(
                  (value) => {
                    this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex]["ID_CANVAS_LIGNE"] = value.data.ID_CANVAS_LIGNE;
                    this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex]["SYNCHRO"] = false;
                    this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex][event.args.datafield] = event.args.value;
                    let plc = (<Array<ParamLigneCanvas>>value.data.PARAM_LIGNE_CANVAS).find(p => p.ID_PARAM == paramLigneCanvas.ID_PARAM);
                    plc.ID_PARAM = paramLigneCanvas.ID_PARAM;
                    (<CanvasLigne>value.data).PARAM_LIGNE_CANVAS.forEach(plc => {
                      this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex]["valid_" + plc.ID_PARAM] = false;
                      this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex]["contenuid_" + plc.ID_PARAM] = plc.ID_PARAM_LIGNE_CANVAS;
                    });
                    this.initCanvasGridWithRenderer(canvas);
                    this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].updatebounddata();
                    this.updatevalidparamlignecanvas(plc, canvas, event);
                  },
                  (error) => {
                    this.alertBoxService.alert({
                      icon: "error",
                      title: error.statusText,
                      text: error.error.message,
                    });
                  }
                );

            } else {
              this.fillCanvasGrid(canvas);
              this.alertBoxService.alert({
                icon: "error",
                title: "Données non valides",
                // text: "Données non valides",
              });
            }
          } else {

            let paramLigneCanvas: ParamLigneCanvas = new ParamLigneCanvas();
            let canvasLigne = new CanvasLigne();
            canvasLigne.ANNULER = event.args.row.ANNULER;
            this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex]["ANNULER"] = event.args.row.ANNULER;
            paramLigneCanvas.ID_PARAM = param.ID_PARAM;
            paramLigneCanvas.CONTENU = await this.getvaleurRouteur(event.args.value, param);
            canvasLigne.PARAM_LIGNE_CANVAS = [];
            paramLigneCanvas.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
            paramLigneCanvas.DATETIME_SAISIE = new Date();
            canvasLigne.PARAM_LIGNE_CANVAS.push(paramLigneCanvas);
            canvasLigne.ID_CANVAS = canvas.ID_CANVAS;
            canvasLigne.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
            canvasLigne.DATETIME_SAISIE = new Date();
            canvas.PARAMS.forEach(p => {
              if (p.ID_PARAM != param.ID_PARAM) {
                let paramLigneCanvas: ParamLigneCanvas = new ParamLigneCanvas();
                paramLigneCanvas.ID_PARAM = p.ID_PARAM;
                paramLigneCanvas.CONTENU = null;
                paramLigneCanvas.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
                paramLigneCanvas.DATETIME_SAISIE = new Date();
                canvasLigne.PARAM_LIGNE_CANVAS.push(paramLigneCanvas);
              }
            });
            this.canvasLigneService.createWithParams(canvasLigne).pipe(takeUntil(this.subscriptions))
              .subscribe(
                (value) => {
                  this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex]["ID_CANVAS_LIGNE"] = value.data.ID_CANVAS_LIGNE;
                  this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex][event.args.datafield] = event.args.value;
                  this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex]["SYNCHRO"] = false;
                  (<CanvasLigne>value.data).PARAM_LIGNE_CANVAS.forEach(plc => {
                    this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex]["valid_" + plc.ID_PARAM] = false;
                    this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex]["contenuid_" + plc.ID_PARAM] = plc.ID_PARAM_LIGNE_CANVAS;
                  });
                  let plc = (<Array<ParamLigneCanvas>>value.data.PARAM_LIGNE_CANVAS).find(p => p.ID_PARAM == paramLigneCanvas.ID_PARAM);
                  plc.ID_PARAM = paramLigneCanvas.ID_PARAM;
                  this.initCanvasGridWithRenderer(canvas);
                  this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].updatebounddata();
                  this.updatevalidparamlignecanvas(plc, canvas, event);

                },
                (error) => {
                  this.alertBoxService.alert({
                    icon: "error",
                    title: error.statusText,
                    text: error.error.message,
                  });
                }
              );
          }

        }

      }
    }

    else {

      if ((<string>event.args.datafield) != "ANNULER" && (<string>event.args.datafield) != "SYNCHRO") {
        let param_id: number; param_id = +(<string>event.args.datafield).split("_")[1];
        let paramLigneCanvas = new ParamLigneCanvas();
        paramLigneCanvas.ID_PARAM = param_id;
        paramLigneCanvas.ID_PARAM_LIGNE_CANVAS = event.args.row["contenuid_" + param_id];
        if (paramLigneCanvas.ID_PARAM_LIGNE_CANVAS)
          this.updatevalidparamlignecanvas(paramLigneCanvas, canvas, event);
      }
    }

  }
  updatevalidparamlignecanvas(paramlignecanvas: ParamLigneCanvas, canvas: Canvas, event: any) {
    this.confirmValid("Valid Param", "is this value valid").then((value) => {
      paramlignecanvas.VALID = value;
      paramlignecanvas.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
      paramlignecanvas.DATETIME_SAISIE = new Date();

      this.paramLigneCanvasService.update(paramlignecanvas).pipe(takeUntil(this.subscriptions))
        .subscribe(
          (apidata) => {
            if (value) {
              if (!this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex].SYNCHRO) {
                let canvasLigne = new CanvasLigne();
                canvasLigne.ID_CANVAS_LIGNE = this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex]["ID_CANVAS_LIGNE"];
                canvasLigne.SYNCHRO = true;
                canvasLigne.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
                canvasLigne.DATETIME_SAISIE = new Date();

                this.canvasLigneService.NonValidParams(canvasLigne.ID_CANVAS_LIGNE).pipe(takeUntil(this.subscriptions)).subscribe(
                  (value) => {
                    if (value.data.length == 0) {
                      this.canvasLigneService.update(canvasLigne).pipe(takeUntil(this.subscriptions))
                        .subscribe(
                          (value) => {
                            this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex].SYNCHRO = true;
                            this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex]["valid_" + paramlignecanvas.ID_PARAM] = value;
                            this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].updatebounddata();
                            this.alertBoxService.alertEdit();
                          },
                          (error) => {
                            this.alertBoxService.alert({
                              icon: "error",
                              title: error.statusText,
                              text: error.error.message,
                            });


                          }
                        );
                    } else {
                      this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex]["valid_" + paramlignecanvas.ID_PARAM] = value;
                      this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].updatebounddata();
                      this.alertBoxService.alertEdit();
                    }
                  },
                  (error) => {
                    this.alertBoxService.alert({
                      icon: "error",
                      title: error.statusText,
                      text: error.error.message,
                    });
                  }
                );
              }
            } else {
              if (!this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex].SYNCHRO) {

                this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex]["valid_" + paramlignecanvas.ID_PARAM] = value;
                this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].updatebounddata();
                this.alertBoxService.alertEdit();
              } else {
                let canvaligne = new CanvasLigne();
                canvaligne.ID_CANVAS_LIGNE = this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex].ID_CANVAS_LIGNE;
                canvaligne.SYNCHRO = false;
                canvaligne.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
                canvaligne.DATETIME_SAISIE = new Date();

                this.canvasLigneService.update(canvaligne).pipe(takeUntil(this.subscriptions))
                  .subscribe(
                    (valueapi) => {
                      this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex].SYNCHRO = false;
                      this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[event.args.rowindex]["valid_" + paramlignecanvas.ID_PARAM] = value;
                      this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].updatebounddata();
                      this.alertBoxService.alertEdit();
                    },
                    (error) => {
                      this.alertBoxService.alert({
                        icon: "error",
                        title: error.statusText,
                        text: error.error.message,
                      });
                    }
                  );

              }
            }
          },
          (error) => {
            this.alertBoxService.alert({
              icon: "error",
              title: error.statusText,
              text: error.error.message,
            });
          }
        );
    }).catch((err) => {
      this.alertBoxService.alert({
        icon: "error",
        title: "err",
        text: err,
      });
    })
  }
  confirmValid(titre: string, question: string): Promise<boolean> {

    const editedSwal = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'ml-3 btn btn-danger'
      },
      buttonsStyling: false
    });

    return new Promise<any>((resolve, reject) => {

      editedSwal.fire({
        title: '<strong>' + titre + '</strong>',
        icon: 'question',
        text: question,
        showCancelButton: true,
        focusConfirm: false,
        confirmButtonText:
          '<i class="fa fa-check"></i> Oui',
        cancelButtonText:
          '<i class="fa fa-close"></i> Non'
      })
        .then((result) => {
          if (result.value) {
            resolve(true);
          } else {
            resolve(false);
          }
        })
        .catch(e => reject(e));
    });

  }

  gridPageChange(event: any, canvas: Canvas) {

  }
  getParamEditorDataAdapter(param: Param) {
    return this.paramService.findQueryDonneeData(param.ID_PARAM).pipe(takeUntil(this.subscriptions));
  }
  async getvaleurAfficher(valeurRouteur: any, param: Param) {
    let vAfficher: string = null;
    if (this.QueryDonneeDataCach.get(param.ID_PARAM) != null) {
      let paramvaleurfromquery = (this.QueryDonneeDataCach.get(param.ID_PARAM)).find(element => element[param.VALEUR_RETOUR] == valeurRouteur);
      if (paramvaleurfromquery) {
        vAfficher = paramvaleurfromquery[param.VALEUR_AFFICHER];
      }

    } else {
      let value = await this.paramService.findQueryDonneeData(param.ID_PARAM).pipe(takeUntil(this.subscriptions)).toPromise();
      if (value.data) {
        this.QueryDonneeDataCach.set(param.ID_PARAM, value.data);
        let paramvaleurfromquery = (<Array<any>>value.data).find(element => element[param.VALEUR_RETOUR] == valeurRouteur);
        if (paramvaleurfromquery) {
          vAfficher = paramvaleurfromquery[param.VALEUR_AFFICHER];
        }
      }
      if (!vAfficher)
        this.alertBoxService.alert({
          icon: "error",
          title: "Query Donne not found",
          text: "",
        });
    }
    return vAfficher;
  }
  async getvaleurRouteur(valeurAfficher: any, param: Param) {
    let vRetour: string = null;
    if (this.QueryDonneeDataCach.get(param.ID_PARAM) != null) {
      let paramvaleurfromquery = (this.QueryDonneeDataCach.get(param.ID_PARAM)).find(element => element[param.VALEUR_AFFICHER] == valeurAfficher);
      if (paramvaleurfromquery) {
        vRetour = paramvaleurfromquery[param.VALEUR_RETOUR];
      }

    } else {
      let value = await this.paramService.findQueryDonneeData(param.ID_PARAM).pipe(takeUntil(this.subscriptions)).toPromise();
      if (value.data) {
        this.QueryDonneeDataCach.set(param.ID_PARAM, value.data);
        let paramvaleurfromquery = (<Array<any>>value.data).find(element => element[param.VALEUR_AFFICHER] == valeurAfficher);
        if (paramvaleurfromquery) {
          vRetour = paramvaleurfromquery[param.VALEUR_RETOUR];
        }
      }
      if (!vRetour) {
        this.alertBoxService.alert({
          icon: "error",
          title: "Requête Donne introuvable ",
          text: "la valeur donnée n'a pas de valeur de retour",
        });
      }

    }

    return vRetour;
  }

  addLigne(canvas: Canvas) {
    if (!this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[0] || this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata[0].ID_CANVAS_LIGNE != null) {
      this.canvasGridsSettings.get(canvas.ID_CANVAS).dataSource;
      let row: any = {};
      row.ID_CANVAS_LIGNE = null;
      row.SYNCHRO = false;
      row.ANNULER = false;
      canvas.PARAMS.forEach(param => {
        row["param_" + param.ID_PARAM] = null;
      });
      this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata = [row].concat(this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata);
      this.initCanvasGridWithRenderer(canvas);
      this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].updatebounddata();
    } else {
      this.alertBoxService.alert({
        text: 'Veuillez s\'il vous plaît remplir tous les champs',
        title: 'Ajout de Canvas Linge',
        icon: 'error'
      });
    }
  }

  rejectSelectedCell(canvas: Canvas) {
    if (
      this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].getselectedcell() && (<string>this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].getselectedcell().datafield).includes('param')) {
      let paramName = canvas.PARAMS.find(p => p.ID_PARAM == +(<string>this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].getselectedcell().datafield).split("_")[1]).LIBELLE_PARAM

      this.selectedcelldata = {
        cell: this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == canvas.ID_CANVAS)].getselectedcell(), canvas, paramName
      }
      this.showpopup = true;

    } else {
      this.alertBoxService.alert({
        icon: "error",
        title: "Aucune cellule n'est sélectionnée ",
        text: "sélectionnez une cellule de paramètre à rejeter",
      });
    }

  }


  addMotif() {
    if (this.formGroup.valid) {
      let param_id = +(<string>this.selectedcelldata.cell.datafield).split("_")[1];
      let contenuid = this.canvasGridsSettings.get(this.selectedcelldata.canvas.ID_CANVAS).source.localdata[this.selectedcelldata.cell.rowindex]["contenuid_" + param_id];
      let motif = new Motif();
      motif.ID_PARAM_CONTENU = contenuid;
      motif.MOTIF = this.formGroup.get('motif').value;
      motif.ID_OP_VERIFIER = this.authService.getDecodedToken().ID_UTILISATEUR;
      motif.DATETIME_VERIFIER = new Date();
      let paramLigneCanvas = new ParamLigneCanvas();
      paramLigneCanvas.ID_PARAM_LIGNE_CANVAS = contenuid;
      paramLigneCanvas.VALID = false;
      paramLigneCanvas.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
      paramLigneCanvas.DATETIME_SAISIE = new Date();
      let canvasLigne = new CanvasLigne();
      canvasLigne.ID_CANVAS_LIGNE = this.canvasGridsSettings.get(this.selectedcelldata.canvas.ID_CANVAS).source.localdata[this.selectedcelldata.cell.rowindex].ID_CANVAS_LIGNE;
      canvasLigne.SYNCHRO = false;
      canvasLigne.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
      canvasLigne.DATETIME_SAISIE = new Date();
      forkJoin([this.motifService.create(motif), this.paramLigneCanvasService.update(paramLigneCanvas), this.canvasLigneService.update(canvasLigne)]).pipe(takeUntil(this.subscriptions)).subscribe(
        (value) => {
          this.showpopup = false;
          this.canvasGridsSettings.get(this.selectedcelldata.canvas.ID_CANVAS).source.localdata[this.selectedcelldata.cell.rowindex].SYNCHRO = false;
          this.canvasGridsSettings.get(this.selectedcelldata.canvas.ID_CANVAS).source.localdata[this.selectedcelldata.cell.rowindex]["valid_" + param_id] = false;
          this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == this.selectedcelldata.canvas.ID_CANVAS)].updatebounddata();
          this.alertBoxService.alertEdit();
          this.formGroup.reset();
        },
        (error) => {
          this.alertBoxService.alert({
            icon: "error",
            title: error.statusText,
            text: error.error.message,
          });
        }
      );
    }
  }

  refreshCanvasGrif(canvas: Canvas) {
    this.fillCanvasGrid(canvas);
  }
  addMultipleLignesFromfile() {
    this.errorsinfile = [];
    let file: File = this.fileInput.nativeElement.files[0];
    const reader = new FileReader();
    reader.onload = async () => {
      let wb: XLSX.WorkBook = XLSX.read(reader.result, { type: "buffer" });
      let dataArray = XLSX.utils.sheet_to_json(wb.Sheets["data"]);
      if (dataArray.length > 0) {
        let obligatorycoll: string[] = Object.keys(this.createcollumsobj(this.canvastoinsertlines));
        let columnNames = [];
        var worksheet = wb.Sheets["data"];
        for (let key in worksheet) {
          let regEx = new RegExp("^\(\\w\)\(1\){1}$");
          if (regEx.test(key) == true) {
            columnNames.push(worksheet[key].v);
          }
        }

        obligatorycoll.sort();
        columnNames.sort();
        let samecolls = true;
        for (let i = 0; i < obligatorycoll.length; i++) {
          if (columnNames[i] !== obligatorycoll[i]) {
            samecolls = false;
          }
        } if (obligatorycoll.length != columnNames.length) { samecolls = false; }

        if (samecolls) {
          //? starting validation for each cell
          let canvalignes = new Array<CanvasLigne>();
          for (let rowindex = 0; rowindex < dataArray.length; rowindex++) {
            const row: { [k: string]: any } = dataArray[rowindex];
            let cl = new CanvasLigne();
            cl.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
            cl.DATETIME_SAISIE = new Date();
            cl.ID_CANVAS = this.canvastoinsertlines.ID_CANVAS;
            cl.PARAM_LIGNE_CANVAS = new Array<ParamLigneCanvas>();
            for (let index = 0; index < obligatorycoll.length; index++) {
              const col = obligatorycoll[index];
              if (col === "annuler") {
                if (typeof row[col] == "boolean") {
                  cl.ANNULER = row[col];
                } else {
                  if (row[col] == undefined) {
                    cl.ANNULER = false;
                  } else {
                    this.errorsinfile.push({ ligne: rowindex + 1, param: col })
                  }
                }
              } else {
                let param = this.canvastoinsertlines.PARAMS.find(p => p.LIBELLE_PARAM === col);
                let plc = new ParamLigneCanvas();
                plc.ID_PARAM = param.ID_PARAM;
                plc.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
                plc.DATETIME_SAISIE = new Date();
                if (row[col] != undefined) {
                  if (param.ID_FORMAT_DONNEE) {
                    if (getValidationFunction(param.ID_FORMAT_DONNEE)(row[col])) {
                      plc.CONTENU = row[col];
                    } else {
                      this.errorsinfile.push({ ligne: rowindex + 1, param: col })
                    }
                  } else {
                    let vRetour: string = await this.getvaleurRouteur(row[col], param);
                    if (vRetour != null) {
                      plc.CONTENU = vRetour;
                    } else {
                      this.errorsinfile.push({ ligne: rowindex + 1, param: col })
                    }
                  }
                } else {
                  plc.CONTENU = null;
                } cl.PARAM_LIGNE_CANVAS.push(plc);
              }
            }
            if (cl.PARAM_LIGNE_CANVAS.length == 0) {
              this.errorsinfile.push({ ligne: rowindex + 1, msg: "all params are null" })
            } canvalignes.push(cl);
          }
          if (this.errorsinfile.length == 0) {
            canvalignes.forEach(canvasLigne => {
              this.canvasLigneService.create(canvasLigne).pipe(takeUntil(this.subscriptions))
                .subscribe(
                  async (value) => {
                    let row: { [k: string]: any } = {};
                    row["ID_CANVAS_LIGNE"] = value.data.ID_CANVAS_LIGNE;
                    row["SYNCHRO"] = false;
                    row["ANNULER"] = value.data.ANNULER;
                    for (let index = 0; index < canvasLigne.PARAM_LIGNE_CANVAS.length; index++) {
                      const plc = canvasLigne.PARAM_LIGNE_CANVAS[index];
                      plc.ID_CANVAS_LIGNE = value.data.ID_CANVAS_LIGNE;
                      let param = this.canvastoinsertlines.PARAMS.find(p => p.ID_PARAM === plc.ID_PARAM);
                      let plcdata = await this.paramLigneCanvasService.create(plc).pipe(takeUntil(this.subscriptions)).toPromise();
                      if (param.ID_FORMAT_DONNEE) {
                        row["param_" + plc.ID_PARAM] = plc.CONTENU;
                      } else {
                        row["param_" + plc.ID_PARAM] = await this.getvaleurAfficher(plc.CONTENU, param);
                      }
                      row["valid_" + plc.ID_PARAM] = false;
                      row["contenuid_" + plc.ID_PARAM] = plcdata.data.ID_PARAM_LIGNE_CANVAS;

                    }
                    this.canvasGridsSettings.get(this.canvastoinsertlines.ID_CANVAS).source.localdata.push(row);
                    this.CanvasGrids.toArray()[this.canvas.findIndex(c => c.ID_CANVAS == this.canvastoinsertlines.ID_CANVAS)].updatebounddata();
                  },
                  (error) => {
                    this.alertBoxService.alert({
                      icon: "error",
                      title: error.statusText,
                      text: error.error.message,
                    });
                  }
                );

            });
            this.excelformGroup.reset();
            this.showmultiplelinespopup = false;
          }
        } else {
          this.alertBoxService.alert({
            icon: 'error',
            title: 'mauvais fichier Excel',
            text: 'le fichier que vous avez téléchargé ne correspond pas au canvas '
          });
        }
      } else {
        this.alertBoxService.alert({
          icon: 'error',
          title: 'Fichier Excel vide ',
          text: 'le fichier que vous avez téléchargé est vide'
        });
      }
    }; reader.readAsArrayBuffer(file);
  }
  addMultipleLigne(canvas: Canvas) {
    this.showmultiplelinespopup = true;
    // this.excel.excelexport((<Array<any>>this.canvasGridsSettings.get(canvas.ID_CANVAS).source.localdata), canvas.LIBELLE);
    this.excel.excelexport([this.createcollumsobj(canvas)], canvas.LIBELLE);
    this.canvastoinsertlines = canvas;
  }
  createcollumsobj(canvas: Canvas) {
    let colstosheet: { [k: string]: null } = {};
    this.canvasGridsSettings.get(canvas.ID_CANVAS).columns.forEach(col => {
      // if (col.datafield != "ID_CANVAS_LIGNE" && col.datafield != "ANNULER" && col.datafield != "SYNCHRO")
      if (col.datafield != "ID_CANVAS_LIGNE" && col.datafield != "SYNCHRO")
        colstosheet[col.text] = null;
    })
    return colstosheet;
  }
}
