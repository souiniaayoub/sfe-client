import { ReactiveFormsModule } from "@angular/forms";
import {
  NgModule,
  NO_ERRORS_SCHEMA,
} from "@angular/core";
import { CommonModule } from "@angular/common";

import { GestionMdmRoutingModule } from "./gestion-mdm-routing.module";
import { ParametrageComponent } from "./parametrage/parametrage.component";
import { ModelComponent } from "./parametrage/model/model.component";
import { CanvasComponent } from "./parametrage/canvas/canvas.component";
import { ParamettersComponent } from "./parametrage/parametters/parametters.component";
import { jqxGridModule } from "jqwidgets-ng/jqxgrid";
import { CreateCanvasFormComponent } from "./parametrage/canvas/create-canvas-form/create-canvas-form.component";
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { DonneesComponent } from './donnees/donnees.component';

@NgModule({
  declarations: [
    ParametrageComponent,
    ModelComponent,
    CanvasComponent,
    ParamettersComponent,
    CreateCanvasFormComponent,
    DonneesComponent
  ],
  imports: [
    CommonModule,
    jqxGridModule,
    GestionMdmRoutingModule,
    ReactiveFormsModule, NgMultiSelectDropDownModule.forRoot()
  ],
  schemas: [NO_ERRORS_SCHEMA],
})
export class GestionMdmModule { }
