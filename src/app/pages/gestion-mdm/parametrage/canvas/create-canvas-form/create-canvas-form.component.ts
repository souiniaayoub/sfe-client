import { Component, OnInit, OnDestroy, Output, EventEmitter } from "@angular/core";
import { Subject } from "rxjs";
import { AuthService } from "../../../../../services/login/auth.service";
import { AlertBoxService } from "../../../../../services/utils/alert-box.service";
import { CanvasService } from "../../../../../services/gestion-mdm/Canvas/canvas.service";
import { ParamService } from "../../../../../services/gestion-mdm/Param/param.service";
import { ModelService } from "../../../../../services/gestion-mdm/Model/model.service";
import { JqxgridFrService } from "../../../../../services/utils/jqxgrid-fr.service";
import { Param } from "../../../../../entities/gestion-mdm/MDM_PARAM";
import {
  FormGroup,
  FormControl,
  FormArray,
  Validators,
  AbstractControl,
} from "@angular/forms";
import { Model } from "../../../../../entities/gestion-mdm/MDM_MODEL";
import { map, takeUntil } from "rxjs/operators";
import { FormatDonneeService } from "../../../../../services/gestion-mdm/FormatDonnee/format-donnee.service";
import { FormatDonnee } from "../../../../../entities/gestion-mdm/MDM_FORMAT_DONNEE";
import { Canvas } from "../../../../../entities/gestion-mdm/MDM_CANVAS";

@Component({
  selector: "app-create-canvas-form",
  templateUrl: "./create-canvas-form.component.html",
  styleUrls: ["./create-canvas-form.component.css"],
})
export class CreateCanvasFormComponent implements OnInit, OnDestroy {
  private subscriptions: Subject<void> = new Subject<void>();
  @Output() addToTable: EventEmitter<Canvas> = new EventEmitter<Canvas>();
  @Output() RefreshTable: EventEmitter<any> = new EventEmitter<any>();
  public formgroup: FormGroup;
  public models: Model[] = null;
  public formatDonnees: FormatDonnee[];
  public query = new FormGroup({
    qdonnebase: new FormControl(null),
    vafficher: new FormControl(null),
    vretour: new FormControl(null),
  });
  public modeldropdownSettings: any = {
    singleSelection: true,
    idField: "ID_MDM_MODEL",
    textField: "LIBELLE",
  };
  public formatdonnedropdownSettings: any = {
    singleSelection: true,
    idField: "ID_FORMAT_DONNEE",
    textField: "LIBELLE_FORMAT_DONNEE",
  };
  constructor(
    public authService: AuthService,
    private canvasService: CanvasService,
    private modelService: ModelService,
    private jqxgridFrService: JqxgridFrService,
    private alertBoxService: AlertBoxService,
    private formatDonneeService: FormatDonneeService
  ) { }
  ngOnDestroy(): void {
    this.subscriptions.next();
    this.subscriptions.complete();
  }
  ngOnInit(): void {
    this.modelService
      .findAll()
      .pipe(takeUntil(this.subscriptions))
      .subscribe(
        (value) => {
          this.models = value.data;
          this.models = value.data;
        },
        (error) => {
          this.alertBoxService.alert({
            icon: "error",
            title: error.statusText,
            text: error.error.message,
          });
        }
      );
    this.formatDonneeService
      .findAll()
      .pipe(takeUntil(this.subscriptions))
      .subscribe(
        (value) => {
          this.formatDonnees = value.data;
          this.formatDonnees = value.data;
        },
        (error) => {
          this.alertBoxService.alert({
            icon: "error",
            title: error.statusText,
            text: error.error.message,
          });
        }
      );
    this.formgroup = new FormGroup({
      canvasgroup: new FormGroup({
        libelleControl: new FormControl(null, [
          Validators.required,
          Validators.maxLength(50),
        ], [this.isUniqueCanvasLibelle]),
        modelControl: new FormControl(null, Validators.required),
      }),
      paramsArray: new FormArray([]),
    });
    this.addParamControl();
  }
  addParamControl() {
    let control = new FormGroup({
      paramlibelle: new FormControl(null, [
        Validators.required,
        Validators.maxLength(50),
      ]),
      valeurpredefini: new FormControl(true),
      // isprimarykey: new FormControl(true),
      formatdonee: new FormControl(null),
      query: new FormGroup({
        qdonnebase: new FormControl(null, [
          Validators.required,
          // Validators.maxLength(150),
        ]),
        vafficher: new FormControl(null, [
          Validators.required,
          Validators.maxLength(50),
        ]),
        vretour: new FormControl(null, [
          Validators.required,
          Validators.maxLength(50),
        ]),
      }),
    });
    (<FormArray>this.formgroup.get("paramsArray")).push(control);
  }
  createCanvas() {
    if (this.formgroup.valid) {
      let canvas: Canvas = new Canvas();
      canvas.ID_MDM_MODEL = (<FormGroup>(
        this.formgroup.controls.canvasgroup
      )).controls.modelControl.value[0].ID_MDM_MODEL;
      canvas.MODEL = (<FormGroup>(
        this.formgroup.controls.canvasgroup
      )).controls.modelControl.value[0];
      canvas.LIBELLE = (<FormGroup>(
        this.formgroup.controls.canvasgroup
      )).controls.libelleControl.value;
      canvas.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
      canvas.DATETIME_SAISIE = new Date();
      canvas.PARAMS = new Array();

      (<FormArray>this.formgroup.get("paramsArray")).controls.forEach(
        (element: FormGroup) => {
          let param = new Param();
          param.LIBELLE_PARAM = element.get("paramlibelle").value;
          param.ID_OP_SAISIE = canvas.ID_OP_SAISIE;
          param.DATETIME_SAISIE = canvas.DATETIME_SAISIE;
          if (element.get("valeurpredefini").value) {
            param.QUERY_DONNE_BASE = element
              .get("query")
              .get("qdonnebase").value;
            param.VALEUR_AFFICHER = element.get("query").get("vafficher").value;
            param.VALEUR_RETOUR = element.get("query").get("vretour").value;
          } else {
            param.ID_FORMAT_DONNEE = element.get("formatdonee").value[0].ID_FORMAT_DONNEE;
          }
          canvas.PARAMS.push(param);
        }
      );
      this.canvasService
        .createWithParams(canvas)
        .pipe(takeUntil(this.subscriptions))
        .subscribe(
          (value) => {
            // this.addToTable.emit(value.data);
            this.formgroup.reset();
            this.formgroup.removeControl("paramsArray");
            this.formgroup.addControl("paramsArray", new FormArray([]));
            this.addParamControl();
            canvas.ID_CANVAS = value.data.ID_CANVAS;
            this.addToTable.emit(canvas);
          },
          (error) => {
            this.alertBoxService.alert({
              icon: "error",
              title: error.statusText,
              text: error.error.message,
            });
          }
        );
    } else {
      this.alertBoxService.alert({
        icon: "error",
        title: "Les données ne sont pas valides",
        // text: "Les données ne sont pas valides",
      });
    }
  }

  changeValeurPredefini(formGroupparam: any) {
    if (!formGroupparam.valeurpredefini.value) {
      formGroupparam.formatdonee.setValidators(Validators.required);
      formGroupparam.query.controls.qdonnebase.clearValidators();
      formGroupparam.query.controls.qdonnebase.updateValueAndValidity();
      formGroupparam.query.controls.vafficher.clearValidators();
      formGroupparam.query.controls.vafficher.updateValueAndValidity();
      formGroupparam.query.controls.vretour.clearValidators();
      formGroupparam.query.controls.vretour.updateValueAndValidity();
    } else {
      formGroupparam.query.controls.qdonnebase.setValidators([
        Validators.required,
        // Validators.maxLength(150),
      ]);
      formGroupparam.query.controls.vafficher.setValidators([
        Validators.required,
        Validators.maxLength(50),
      ]);
      formGroupparam.query.controls.vretour.setValidators([
        Validators.required,
        Validators.maxLength(50),
      ]);
      formGroupparam.formatdonee.clearValidators();
      formGroupparam.formatdonee.updateValueAndValidity();
    }
  }

  //casting for template errors
  asFormArray(formarray: AbstractControl): FormArray {
    return <FormArray>formarray;
  }
  asFormGroup(formarray: AbstractControl): FormGroup {
    return <FormGroup>formarray;
  }
  isUniqueCanvasLibelle = (control: FormControl) => {
    return this.canvasService.findHasLibelle(control.value).pipe(
      map((response) => {
        return response.data.length === 0 ? null : { uniqueLibelle: true };
      })
    );
  };
  refreshTable() {
    this.RefreshTable.emit();
  }
}
