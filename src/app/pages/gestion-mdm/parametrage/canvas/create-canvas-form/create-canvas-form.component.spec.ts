import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCanvasFormComponent } from './create-canvas-form.component';

describe('CreateCanvasFormComponent', () => {
  let component: CreateCanvasFormComponent;
  let fixture: ComponentFixture<CreateCanvasFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateCanvasFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCanvasFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
