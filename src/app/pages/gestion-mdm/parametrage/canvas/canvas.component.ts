import { FormGroup } from "@angular/forms";
import { Canvas } from "./../../../../entities/gestion-mdm/MDM_CANVAS";
import {
  AfterViewInit,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import { Subject } from "rxjs";
import { AuthService } from "../../../../services/login/auth.service";
import { AlertBoxService } from "../../../../services/utils/alert-box.service";
import { CanvasService } from "../../../../services/gestion-mdm/Canvas/canvas.service";
import { ParamService } from "../../../../services/gestion-mdm/Param/param.service";
import { JqxgridFrService } from "../../../../services/utils/jqxgrid-fr.service";
import { jqxGridComponent } from "jqwidgets-ng/jqxgrid";

import { takeUntil } from "rxjs/operators";
import { Param } from '../../../../entities/gestion-mdm/MDM_PARAM';
import { ModelService } from "../../../../services/gestion-mdm/Model/model.service";
import { Model } from '../../../../entities/gestion-mdm/MDM_MODEL';

@Component({
  selector: "app-canvas",
  templateUrl: "./canvas.component.html",
  styleUrls: ["./canvas.component.scss"],
})
export class CanvasComponent implements OnInit, OnDestroy, AfterViewInit {
  // @ViewChild("grid", { static: false }) Grid: jqxGridComponent;
  @ViewChild("Grid") Grid: jqxGridComponent;
  public createpermission: boolean;
  public listpermission: boolean;
  // canvas table variables
  public canvasAdapter: any;
  public columns: any[];
  public localization: any;
  public source: any;
  public rowdetailstemplate: any;
  public nestedGrids: any[];
  // param table variables
  private paramsSource: any;
  private paramsDataAdapter: any;

  private subscriptions: Subject<void> = new Subject<void>();
  public createcanvasform: FormGroup;
  constructor(
    public authService: AuthService,
    private canvasService: CanvasService,
    private paramService: ParamService,
    private modelService: ModelService,
    private jqxgridFrService: JqxgridFrService,
    private alertBoxService: AlertBoxService
  ) { }
  ngOnDestroy(): void {
    this.subscriptions.next();
    this.subscriptions.complete();
  }
  ngAfterViewInit(): void {
    if (this.listpermission) {
      this.Grid.setcolumnproperty("LIBELLE", "editable", true);
      this.Grid.setcolumnproperty("ACTIF", "editable", true);
    }
  }
  ngOnInit(): void {
    this.createpermission = true;
    // this.listpermission = false;
    this.listpermission = true;
    if (this.listpermission) {
      this.initGrid();
      this.fillGrid();
    }
  }

  private initGrid(): void {
    this.nestedGrids = new Array();
    this.rowdetailstemplate = {
      rowdetails: '<div id="nestedGrid" style="margin: 10px;"></div>',
      rowdetailsheight: 220,
      rowdetailshidden: true,
    };

    this.paramsSource = {
      datafields: [
        { name: "ID_PARAM", type: "int" },
        { name: "LIBELLE_PARAM", type: "string" },
        { name: "IS_PRIMARY_KEY", type: "boolean" },

        { name: "QUERY_DONNE_BASE", type: "string" },
        { name: "VALEUR_AFFICHER", type: "string" },
        { name: "VALEUR_RETOUR", type: "string" },
        { name: "ID_FORMAT_DONNEE", type: "int" },
      ],
      datatype: "array",
      localdata: [],
    };

    this.source = {
      datafields: [
        { name: "ID_CANVAS", type: "int" },
        { name: "ID_MDM_MODEL", type: "int" },
        { name: "LIBELLE", type: "string" },
        { name: "MODEL_LIBELLE", type: "string" },
        { name: "ACTIF", type: "boolean" },
      ],
      id: "ID_CANVAS",
      datatype: "array",
      localdata: [],
    };
    this.paramsDataAdapter = new jqx.dataAdapter(this.paramsSource, {
      autoBind: true,
    });
    this.canvasAdapter = new jqx.dataAdapter(this.source);
    this.columns = [
      {
        text: "ID Canvas",
        datafield: "ID_CANVAS",
        width: "10%",
        editable: false,
        cellsalign: "center",
        align: "center",
      },
      {
        text: "Libellé",
        datafield: "LIBELLE",
        editable: false,
        cellsalign: "center",
        align: "center",
        // filtertype: "input",
      },
      {
        text: "Modèle",
        datafield: "MODEL_LIBELLE",
        width: "20%",
        editable: false,
        cellsalign: "center",
        align: "center",
        // filtertype: "input",
      },
      {
        text: "Actif",
        datafield: "ACTIF",
        width: "20%",
        columntype: "checkbox",
        filtertype: "checkedlist",
        editable: false,
        cellsalign: "center",
        align: "center",
      },
    ];
  }

  private fillGrid(): void {
    this.localization = this.jqxgridFrService.getLocalization("fr");
    this.source.localdata = [];
    this.paramsDataAdapter.records = [];
    this.canvasService
      .findAllIncludeParamModel()
      .pipe(takeUntil(this.subscriptions))
      .subscribe(
        (value) => {
          value.data.forEach((element: Canvas) => {
            this.source.localdata.push({
              ...element,
              MODEL_LIBELLE: element.MODEL.LIBELLE,
            });
          });
          value.data.forEach((canvas) => {
            if (canvas.PARAMS)
              canvas.PARAMS.forEach((param: Param) => {
                this.paramsDataAdapter.records.push({
                  ...param,
                  ID_CANVAS: canvas.ID_CANVAS,
                });
              });
          });
          this.Grid.updatebounddata();
        },
        (error) => {
          this.alertBoxService.alert({
            icon: "error",
            title: error.statusText,
            text: error.error.message,
          });
        }
      );
  }
  initRowDetails = (
    index: number,
    parentElement: any,
    gridElement: any,
    record: any
  ): void => {
    if (record) {
      let id = record.uid.toString();
      let nestedGridContainer = parentElement.children[0];
      this.nestedGrids[index] = nestedGridContainer;
      let filtergroup = new jqx.filter();
      let filter_or_operator = 1;
      let filtervalue = id;
      let filtercondition = "equal";
      let filter = filtergroup.createfilter(
        "stringfilter",
        filtervalue,
        filtercondition
      );
      // fill the params depending on the id.
      let params = this.paramsDataAdapter.records;
      let paramsbyid = [];
      for (let i = 0; i < params.length; i++) {
        let result = filter.evaluate(params[i]["ID_CANVAS"]);
        if (result) paramsbyid.push(params[i]);
      }
      let paramsSource = {
        datafields: [
          { name: "ID_PARAM", type: "int" },
          { name: "LIBELLE_PARAM", type: "string" },
          { name: "IS_PRIMARY_KEY", type: "boolean" },

          { name: "QUERY_DONNE_BASE", type: "string" },
          { name: "VALEUR_AFFICHER", type: "string" },
          { name: "VALEUR_RETOUR", type: "string" },
          { name: "ID_FORMAT_DONNEE", type: "int" },
          { name: "ID_CANVAS", type: "int" },
        ],
        id: "ID_PARAM",
        localdata: paramsbyid,
      };
      let nestedGridAdapter = new jqx.dataAdapter(paramsSource);
      if (nestedGridContainer != null) {
        let settings = {

          theme: "bootstrap",
          width: "90%",
          autoheight: true,

          source: nestedGridAdapter,
          columns: [
            {
              text: "ID Paramètre",
              datafield: "ID_PARAM",
              editable: false,
              cellsalign: "center",
              width: 100,
              align: "center",
            },
            {
              text: "Libellé",
              datafield: "LIBELLE_PARAM",
              editable: false,
              cellsalign: "center",
              width: 150,
              align: "center",
            },
            {
              text: "is primary key",
              datafield: "IS_PRIMARY_KEY",
              editable: false,
              cellsalign: "center", columntype: "checkbox",
              filtertype: "checkedlist",

              width: 150,
              align: "center",
            },
            {
              text: "Requête Donnée de base",
              datafield: "QUERY_DONNE_BASE",
              editable: false,
              cellsalign: "center",
              align: "center",
              width: 200,
            },
            {
              text: "Valeur afficher",
              datafield: "VALEUR_AFFICHER",
              editable: false,
              cellsalign: "center",
              width: 150,
              align: "center",
            },
            {
              text: "Valeur retour",
              datafield: "VALEUR_RETOUR",
              editable: false,
              cellsalign: "center",
              width: 150,
              align: "center",
            },
            {
              text: "ID Format Données",
              datafield: "ID_FORMAT_DONNEE",
              width: 150,
              editable: false,
              cellsalign: "center",
              align: "center",
            },

          ],
        };

        jqwidgets.createInstance(
          `#${nestedGridContainer.id}`,
          "jqxGrid",
          settings
        );
      }
    }
  };
  ready = (): void => {
    if (this.Grid) this.Grid.showrowdetails(1);
  };

  gridOnCellEndEdit(event: any): void {
    if (event.args.value != event.args.oldvalue) {
      let canvas: Canvas = new Canvas();
      canvas.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
      canvas.DATETIME_SAISIE = new Date();
      canvas.ID_CANVAS = this.source.localdata[event.args.rowindex].ID_CANVAS;
      if (event.args.datafield == "ACTIF") {
        canvas.ACTIF = event.args.value;
      } else {
        canvas.LIBELLE = event.args.value;
      }
      if (canvas.LIBELLE == undefined || (canvas.LIBELLE.length <= 50 && canvas.LIBELLE != "")) {
        if (canvas.ACTIF == undefined || canvas.ACTIF == false) {
          if (canvas.LIBELLE != undefined) {
            this.canvasService
              .findHasLibelle(canvas.LIBELLE)
              .pipe(takeUntil(this.subscriptions))
              .subscribe(
                (value) => {
                  if (value.data.length === 0) {
                    this.canvasService
                      .update(canvas)
                      .pipe(takeUntil(this.subscriptions))
                      .subscribe(
                        (value) => {
                          if (value.data > 0) {
                            this.source.localdata[event.args.rowindex][event.args.datafield] =
                              event.args.value;
                            this.Grid.updatebounddata();
                            this.alertBoxService.alertEdit();
                          }
                        },
                        (error) => {
                          this.alertBoxService.alert({
                            icon: "error",
                            title: error.statusText,
                            text: error.error.message,
                          });
                        }
                      );
                  } else {
                    this.alertBoxService.alert({
                      icon: "error",
                      title: "Libellé unique",
                      text: "Canvas Libellé doit être unique",
                    });
                    this.Grid.updatebounddata();
                  }
                },
                (error) => {
                  this.alertBoxService.alert({
                    icon: "error",
                    title: error.statusText,
                    text: error.error.message,
                  });
                  this.Grid.updatebounddata();
                }
              );
          } else {
            this.canvasService
              .update(canvas)
              .pipe(takeUntil(this.subscriptions))
              .subscribe(
                (value) => {
                  if (value.data > 0) {
                    this.source.localdata[event.args.rowindex][event.args.datafield] =
                      event.args.value;
                    this.Grid.updatebounddata();
                    this.canvasService.findAllBelongsTo(this.source.localdata[event.args.rowindex].ID_MDM_MODEL).pipe(takeUntil(this.subscriptions))
                      .subscribe(
                        (value) => {
                          let hasOneActiveCanvas = false;
                          <Array<Canvas>>(value.data).forEach((canvas: Canvas) => {
                            let thiscanvashasPK = false;
                            if (canvas.ACTIF) {
                              hasOneActiveCanvas = true;
                            }
                          });
                          if (!hasOneActiveCanvas) {
                            let model = new Model();
                            model.ID_MDM_MODEL = this.source.localdata[event.args.rowindex].ID_MDM_MODEL;
                            model.ACTIF = false;
                            this.modelService
                              .update(model)
                              .pipe(takeUntil(this.subscriptions))
                              .subscribe(
                                (value) => {
                                },
                                (error) => {
                                  this.alertBoxService.alert({
                                    icon: "error",
                                    title: error.statusText,
                                    text: error.error.message,
                                  });
                                  this.Grid.updatebounddata();
                                }
                              );
                          }
                        },
                        (error) => {
                          this.alertBoxService.alert({
                            icon: "error",
                            title: error.statusText,
                            text: error.error.message,
                          });
                          this.Grid.updatebounddata();
                        }
                      );
                    this.alertBoxService.alertEdit();
                  }
                },
                (error) => {
                  this.alertBoxService.alert({
                    icon: "error",
                    title: error.statusText,
                    text: error.error.message,
                  });
                }
              );
          }

        } else {
          this.canvasService.findOneIncludeParam(canvas.ID_CANVAS).pipe(takeUntil(this.subscriptions))
            .subscribe(
              (value) => {
                let thiscanvashasPK = false;
                if (value.data.PARAMS)
                  <Array<Param>>(value.data.PARAMS).forEach(param => {
                    if (param.IS_PRIMARY_KEY) {
                      thiscanvashasPK = true;
                      return;
                    }
                  });
                if (thiscanvashasPK && value.data.PARAMS.length > 0) {
                  this.canvasService
                    .update(canvas)
                    .pipe(takeUntil(this.subscriptions))
                    .subscribe(
                      (value) => {
                        if (value.data > 0) {
                          this.source.localdata[event.args.rowindex][event.args.datafield] =
                            event.args.value;
                          this.Grid.updatebounddata();
                          this.alertBoxService.alertEdit();
                        }
                      },
                      (error) => {
                        this.alertBoxService.alert({
                          icon: "error",
                          title: error.statusText,
                          text: error.error.message,
                        });
                      }
                    );
                } else {
                  if (value.data.PARAMS.length > 0)
                    this.alertBoxService.alert({
                      icon: "error",
                      title: "Vous ne pouvez pas activer ce canvas",
                      text: "ce canvas n'a pas de clé primaire ",
                    });
                  if (value.data.PARAMS.length == 0)
                    this.alertBoxService.alert({
                      icon: "error",
                      title: "Vous ne pouvez pas activer ce canvas",
                      text: "ce canvas n'a aucun paramètre",
                    });
                  this.fillGrid();
                }
              },
              (error) => {
                this.alertBoxService.alert({
                  icon: "error",
                  title: error.statusText,
                  text: error.error.message,
                });
                this.Grid.updatebounddata();
              }
            );
        }
      } else {
        if (!(canvas.LIBELLE.length <= 50))
          this.alertBoxService.alert({
            icon: "error",
            title: "Longueur Libellé",
            text: "Canvas Libellé doit comporter moins de 50 caractères",
          });
        if (!(canvas.LIBELLE != ""))
          this.alertBoxService.alert({
            icon: "error",
            title: "Longueur Libellé",
            text: "Canvas Libellé ne peut pas être vide",
          });
        this.fillGrid();
      }
    }
  }
  addToTable(canvas: Canvas) {
    this.source.localdata.push({
      ...canvas,
      MODEL_LIBELLE: canvas.MODEL.LIBELLE,
      ACTIF: canvas.ACTIF
    });
    if (canvas.PARAMS)
      canvas.PARAMS.forEach((param: Param) => {
        this.paramsDataAdapter.records.push({
          ...param,
          ID_CANVAS: canvas.ID_CANVAS,
        });
      });
    this.Grid.updatebounddata();
  }
  refresh(): void {
    this.fillGrid();
  }
}
