import {
  Component,
  OnInit,
  ViewChild,
  OnDestroy,
  AfterViewInit,
} from "@angular/core";
import { jqxGridComponent } from "jqwidgets-ng/jqxgrid";
import { Subject } from "rxjs";
import { map, takeUntil } from "rxjs/operators";
import { JqxgridFrService } from "../../../../services/utils/jqxgrid-fr.service";
import { ModelService } from "../../../../services/gestion-mdm/Model/model.service";
import { AlertBoxService } from "../../../../services/utils/alert-box.service";
import { Model } from "../../../../entities/gestion-mdm/MDM_MODEL";
import { AuthService } from "../../../../services/login/auth.service";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { CanvasService } from '../../../../services/gestion-mdm/Canvas/canvas.service';
import { Canvas } from "../../../../entities/gestion-mdm/MDM_CANVAS";

@Component({
  selector: "app-model",
  templateUrl: "./model.component.html",

  styleUrls: ["./model.component.scss"],
})
export class ModelComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild("grid") Grid: jqxGridComponent;
  public dataAdapter: any;
  public columns: any[];
  public localization: any = null;
  private dataSource: any;
  private subscriptions: Subject<void> = new Subject<void>();
  public createModelForm: FormGroup;
  public createpermission: boolean;
  public listpermission: boolean;
  constructor(
    private canvasService: CanvasService,
    private jqxgridFrService: JqxgridFrService,
    private modelService: ModelService,
    private alertBoxService: AlertBoxService,
    public authService: AuthService
  ) { }

  ngOnInit(): void {
    this.initGrid();
    this.fillGrid();
    this.initCreateModelForm();
    this.createpermission = true;
    this.listpermission = true;
  }
  ngOnDestroy(): void {
    this.subscriptions.next();
    this.subscriptions.complete();
  }
  ngAfterViewInit(): void {
    this.Grid.setcolumnproperty("LIBELLE", "editable", true);
    this.Grid.setcolumnproperty("ACTIF", "editable", true);
  }

  gridOnCellEndEdit(event: any): void {
    let model = new Model();
    model.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
    model.DATETIME_SAISIE = new Date();

    let modelBeforeEdit = new Model();
    if (event.args.datafield === "ACTIF")
      modelBeforeEdit.ACTIF = event.args.oldvalue;
    else if (event.args.datafield === "LIBELLE")
      modelBeforeEdit.LIBELLE = event.args.oldvalue;
    if (event.args.datafield === "ACTIF") {
      model.ACTIF = event.args.value;
    } else {
      if (event.args.datafield === "LIBELLE") {
        model.LIBELLE = event.args.value;
      }
    }

    if (
      model.ACTIF != modelBeforeEdit.ACTIF ||
      model.LIBELLE != modelBeforeEdit.LIBELLE
    ) {
      if (model.LIBELLE == undefined || (model.LIBELLE.length <= 50 && model.LIBELLE != "")) {
        model.ID_MDM_MODEL =
          this.dataSource.localdata[event.args.rowindex].ID_MDM_MODEL;
        model.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
        model.DATETIME_SAISIE = new Date();
        // in case of changing model actif model.LIBELLE is null so is unique and its noy null in db
        if (model.ACTIF == undefined || model.ACTIF == false) {
          this.modelService
            .findHasLibelle(model.LIBELLE)
            .pipe(takeUntil(this.subscriptions))
            .subscribe(
              (value) => {
                if (value.data.length === 0) {
                  this.modelService
                    .update(model)
                    .pipe(takeUntil(this.subscriptions))
                    .subscribe(
                      (value) => {
                        if (value.data > 0) {
                          this.dataSource.localdata[event.args.rowindex][
                            event.args.datafield
                          ] = event.args.value;
                          this.Grid.updatebounddata();
                          this.alertBoxService.alertEdit();
                        }
                      },
                      (error) => {
                        this.alertBoxService.alert({
                          icon: "error",
                          title: error.statusText,
                          text: error.error.message,
                        });
                        this.Grid.updatebounddata();
                      }
                    );
                } else {
                  this.alertBoxService.alert({
                    icon: "error",
                    title: "Libellé unique",
                    text: "Le modèle Libellé doit être unique ",
                  });
                  this.Grid.updatebounddata();
                }
              },
              (error) => {
                this.alertBoxService.alert({
                  icon: "error",
                  title: error.statusText,
                  text: error.error.message,
                });
                this.Grid.updatebounddata();
              }
            );
        } else {
          this.canvasService.findAllBelongsTo(model.ID_MDM_MODEL).pipe(takeUntil(this.subscriptions))
            .subscribe(
              (value) => {
                let allcanvashasPK = true;
                let hasOneActiveCanvas = false;
                <Array<Canvas>>(value.data).forEach((canvas: Canvas) => {
                  let thiscanvashasPK = false;
                  if (canvas.ACTIF) {
                    hasOneActiveCanvas = true;
                  }
                  canvas.PARAMS.forEach(param => {
                    if (param.IS_PRIMARY_KEY) {
                      thiscanvashasPK = true;
                      return;
                    }
                  });
                  if (!thiscanvashasPK) {
                    allcanvashasPK = false;
                    return;
                  }
                });
                if (allcanvashasPK && value.data.length > 0) {
                  if (hasOneActiveCanvas) {
                    this.modelService
                      .update(model)
                      .pipe(takeUntil(this.subscriptions))
                      .subscribe(
                        (value) => {
                          if (value.data > 0) {
                            this.dataSource.localdata[event.args.rowindex][
                              event.args.datafield
                            ] = event.args.value;
                            this.Grid.updatebounddata();
                            this.alertBoxService.alertEdit();
                          }
                        },
                        (error) => {
                          this.alertBoxService.alert({
                            icon: "error",
                            title: error.statusText,
                            text: error.error.message,
                          });
                          this.Grid.updatebounddata();
                        }
                      );
                  } else {
                    this.alertBoxService.alert({
                      icon: "error",
                      title: "Vous ne pouvez pas activer ce modèle ",
                      text: "Le modèle n'a aucun de canvas actif ",
                    });
                    this.fillGrid();
                  }
                } else {
                  if (value.data.length > 0)
                    this.alertBoxService.alert({
                      icon: "error",
                      title: "Vous ne pouvez pas activer ce modèle",
                      text: "un canvas n'a pas de clé primaire ",
                    });
                  if (value.data.length == 0)
                    this.alertBoxService.alert({
                      icon: "error",
                      title: "Vous ne pouvez pas activer ce modèle",
                      text: "Le modèle n'a aucun canvas",
                    });
                  this.fillGrid();
                }
              },
              (error) => {
                this.alertBoxService.alert({
                  icon: "error",
                  title: error.statusText,
                  text: error.error.message,
                });
                this.Grid.updatebounddata();
              }
            );
        }
      } else {
        if (!(model.LIBELLE.length <= 50))
          this.alertBoxService.alert({
            icon: "error",
            title: "Longueur Libellé",
            text: "Le modèle Libellé doit comporter moins de 50 caractères ",
          });
        if (!(model.LIBELLE != ""))
          this.alertBoxService.alert({
            icon: "error",
            title: "Longueur Libellé",
            text: "Le modèle Libellé ne peut pas être vide ",
          });
        this.fillGrid();
      }
    }
  }
  public gridPageChange(event: any): void {
    let paginginfo: any = this.Grid.getpaginginformation();
    if (parseInt(paginginfo.pagenum) == paginginfo.pagescount - 1) {
      this.fillGrid(paginginfo.pagescount);
    }
  }
  private initGrid(): void {
    this.localization = this.jqxgridFrService.getLocalization("fr");

    this.dataSource = {
      datatype: "array",
      datafields: [
        { name: "ID_MDM_MODEL", type: "int" },
        { name: "LIBELLE", type: "string" },
        { name: "ACTIF", type: "boolean" },
      ],
      localdata: [],
    };
    this.dataAdapter = new jqx.dataAdapter(this.dataSource);
    this.columns = [
      {
        text: "ID Modèle",
        datafield: "ID_MDM_MODEL",
        width: "100",
        editable: false,
        cellsalign: "center",
        align: "center",
      },
      {
        text: "Libellé",
        datafield: "LIBELLE",
        editable: false,
        cellsalign: "center",
        align: "center",
        filtertype: "input",
      },
      {
        text: "Actif",
        datafield: "ACTIF",
        width: "150",
        columntype: "checkbox",
        filtertype: "checkedlist",
        editable: false,
        cellsalign: "center",
        align: "center",
      },
    ];
  }
  private fillGrid(startIndex: number = 0): void {
    let pagesize: number = 20;
    if (this.Grid) {
      pagesize = this.Grid.getpaginginformation().pagesize;
    }

    this.modelService
      .findAll()
      .pipe(takeUntil(this.subscriptions))
      .subscribe(
        (value) => {
          if (this.dataSource.localdata.length <= value.data.length) {
            this.dataSource.localdata = value.data;
            this.Grid.updatebounddata();
          }
        },
        (error) => {
          this.alertBoxService.alert({
            icon: "error",
            title: error.statusText,
            text: error.error.message,
          });
        }
      );
  }
  initCreateModelForm(): void {
    this.createModelForm = new FormGroup({
      modellibelle: new FormControl(
        null,
        [Validators.required, Validators.maxLength(50)],
        [this.isUniqueModelLibelle]
      ),
    });
  }
  createModel(): void {
    if (this.createModelForm.valid) {
      let model = new Model();
      model.LIBELLE = this.createModelForm.value.modellibelle;

      model.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
      model.DATETIME_SAISIE = new Date();

      this.modelService
        .create(model)
        .pipe(takeUntil(this.subscriptions))
        .subscribe(
          (value) => {
            this.dataSource.localdata.push(value.data);
            this.Grid.updatebounddata();
            this.alertBoxService.alertCreate();
            this.createModelForm.reset();

          },
          (error) => {
            this.alertBoxService.alert({
              icon: "error",
              title: error.statusText,
              text: error.error.message,
            });
          }
        );
    }
  }
  refresh(): void {
    this.fillGrid();
  }
  isUniqueModelLibelle = (control: FormControl) => {
    return this.modelService.findHasLibelle(control.value).pipe(
      map((response) => {
        return response.data.length === 0 ? null : { uniqueLibelle: true };
      })
    );
  };
}
