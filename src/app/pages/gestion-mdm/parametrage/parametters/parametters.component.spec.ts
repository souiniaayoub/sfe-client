import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParamettersComponent } from './parametters.component';

describe('ParamettersComponent', () => {
  let component: ParamettersComponent;
  let fixture: ComponentFixture<ParamettersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParamettersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParamettersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
