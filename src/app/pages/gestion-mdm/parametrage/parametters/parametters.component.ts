import {
  AfterViewInit,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import { Subject } from "rxjs"; import { ParamService } from '../../../../services/gestion-mdm/Param/param.service';
import { JqxgridFrService } from "../../../../services/utils/jqxgrid-fr.service";
import { jqxGridComponent } from "jqwidgets-ng/jqxgrid";
import { AuthService } from "../../../../services/login/auth.service";
import { takeUntil } from "rxjs/operators";
import { Param } from '../../../../entities/gestion-mdm/MDM_PARAM';
import { AlertBoxService } from '../../../../services/utils/alert-box.service';
import { ModelService } from '../../../../services/gestion-mdm/Model/model.service';
import { Model } from '../../../../entities/gestion-mdm/MDM_MODEL';
import { Canvas } from '../../../../entities/gestion-mdm/MDM_CANVAS';
import { CanvasService } from '../../../../services/gestion-mdm/Canvas/canvas.service';
import { FormGroup, FormControl } from '@angular/forms';
@Component({
  selector: "app-parametters",
  templateUrl: "./parametters.component.html",
  styleUrls: ["./parametters.component.scss"],
})
export class ParamettersComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild("Grid") Grid: jqxGridComponent;
  public listpermission: boolean;
  public columns: any;
  public localization: any;
  private source: any;
  public dataAdapter: any;
  private subscriptions: Subject<void> = new Subject<void>();
  public models: Model[] = null;
  public selectedCanvas: Array<any> = [];
  public form: FormGroup;
  // public showgrid = false;
  public modeldropdownSettings: any = {
    singleSelection: true,
    idField: "ID_MDM_MODEL",
    textField: "LIBELLE",
  };
  public canvas: Canvas[] = null;
  public canvasdropdownSettings: any = {
    singleSelection: true,
    idField: "ID_CANVAS",
    textField: "LIBELLE",
  };
  constructor(
    private canvasService: CanvasService,
    private jqxgridFrService: JqxgridFrService, private paramService: ParamService, private alertBoxService: AlertBoxService, private modelService: ModelService,

    public authService: AuthService) { }
  ngOnDestroy(): void {
    this.subscriptions.next();
    this.subscriptions.complete();
  }
  ngAfterViewInit(): void {
    if (this.listpermission) {
      this.Grid.setcolumnproperty("LIBELLE_PARAM", "editable", true);
      this.Grid.setcolumnproperty("IS_PRIMARY_KEY", "editable", true);
    }
  }

  ngOnInit(): void {
    // this.listpermission = false;
    this.listpermission = true;
    this.modelService
      .findAll()
      .pipe(takeUntil(this.subscriptions))
      .subscribe(
        (value) => {
          this.models = value.data;
          this.models = value.data;
        },
        (error) => {
          this.alertBoxService.alert({
            icon: "error",
            title: error.statusText,
            text: error.error.message,
          });
        }
      );
    if (this.listpermission)
      this.initGrid();
    this.form = new FormGroup({
      model: new FormControl(null),

      canvas: new FormControl(null)
    });
  }

  async gridOnCellEndEdit(event: any) {
    if (event.args.value != event.args.oldvalue && event.args.datafield === "LIBELLE_PARAM") {
      let param = new Param();
      param.LIBELLE_PARAM = event.args.value;
      param.ID_PARAM = event.args.row.ID_PARAM;
      param.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
      param.DATETIME_SAISIE = new Date();

      this.paramService.update(param)
        .pipe(takeUntil(this.subscriptions))
        .subscribe(
          (value) => {
            if (value.data > 0) {
              this.source.localdata[event.args.rowindex][
                event.args.datafield
              ] = event.args.value;
              this.Grid.updatebounddata();
              this.alertBoxService.alertEdit();
            }
          },
          (error) => {
            this.alertBoxService.alert({
              icon: "error",
              title: error.statusText,
              text: error.error.message,
            });
            this.Grid.updatebounddata();
          }
        );
    }
    if (event.args.value != event.args.oldvalue && event.args.datafield === "IS_PRIMARY_KEY") {
      let param = new Param();
      param.IS_PRIMARY_KEY = event.args.value;
      param.ID_PARAM = event.args.row.ID_PARAM;
      param.ID_OP_SAISIE = this.authService.getDecodedToken().ID_UTILISATEUR;
      param.DATETIME_SAISIE = new Date();

      if (param.IS_PRIMARY_KEY) {
        let hasonotherPK = false;
        let apidata = await this.canvasService.HasPrimaryKey(this.form.get('canvas').value[0].ID_CANVAS).pipe(takeUntil(this.subscriptions)).toPromise();
        if ((<Array<Canvas>>apidata.data).length > 0)
          hasonotherPK = true;
        if (!hasonotherPK) {
          this.paramService.update(param)
            .pipe(takeUntil(this.subscriptions))
            .subscribe(
              (value) => {
                if (value.data > 0) {
                  this.source.localdata[event.args.rowindex][
                    event.args.datafield
                  ] = event.args.value;
                  this.canvasService.findAllBelongsTo(this.form.get('model').value[0].ID_MDM_MODEL).pipe(takeUntil(this.subscriptions))
                    .subscribe(
                      (value) => {
                        this.canvas = value.data;
                        this.canvas = value.data;
                      },
                      (error) => {
                        this.alertBoxService.alert({
                          icon: "error",
                          title: error.statusText,
                          text: error.error.message,
                        });
                      }
                    );

                  this.Grid.updatebounddata();
                  this.alertBoxService.alertEdit();
                }
              },
              (error) => {
                this.alertBoxService.alert({
                  icon: "error",
                  title: error.statusText,
                  text: error.error.message,
                });
                this.Grid.updatebounddata();
              }
            );

        } else {
          this.alertBoxService.alert({
            icon: "error",
            title: "erreur de clé primaire",
            text: "Ce Canvas a déjà une clé primaire ",
          });
          this.fillGrid(this.form.get('canvas').value[0].ID_CANVAS);
        }
      } else {
        this.paramService.update(param)
          .pipe(takeUntil(this.subscriptions))
          .subscribe(
            (value) => {
              if (value.data > 0) {
                this.source.localdata[event.args.rowindex][
                  event.args.datafield
                ] = event.args.value;
                this.Grid.updatebounddata();
                // this.alertBoxService.alertEdit();
              }

              let selectedcanvas = this.canvas.find(c => c.ID_CANVAS == this.form.get('canvas').value[0].ID_CANVAS);
              let hasonotherPK = false;
              selectedcanvas.PARAMS.forEach(p => {
                if (p.IS_PRIMARY_KEY && p.ID_PARAM != param.ID_PARAM) {
                  hasonotherPK = true;
                }
              });
              if (!hasonotherPK) {
                selectedcanvas.ACTIF = false;
                this.canvasService.update(selectedcanvas).pipe(takeUntil(this.subscriptions))
                  .subscribe(
                    (value) => {

                    },
                    (error) => {
                      this.alertBoxService.alert({
                        icon: "error",
                        title: error.statusText,
                        text: error.error.message,
                      });
                      this.Grid.updatebounddata();
                    }
                  );
                let model = new Model();
                model.ID_MDM_MODEL = selectedcanvas.ID_MDM_MODEL;
                model.ACTIF = false;
                this.modelService.update(model).pipe(takeUntil(this.subscriptions))
                  .subscribe(
                    (value) => {
                    },
                    (error) => {
                      this.alertBoxService.alert({
                        icon: "error",
                        title: error.statusText,
                        text: error.error.message,
                      });
                      this.Grid.updatebounddata();
                    }
                  );
              }
            },
            (error) => {
              this.alertBoxService.alert({
                icon: "error",
                title: error.statusText,
                text: error.error.message,
              });
              this.Grid.updatebounddata();
            }
          );
      }
    }
  }
  private initGrid(): void {
    this.localization = this.jqxgridFrService.getLocalization("fr");

    this.source = {
      datafields: [
        { name: "ID_PARAM", type: "int" },
        { name: "LIBELLE_PARAM", type: "string" },
        { name: "IS_PRIMARY_KEY", type: "boolean" },
        { name: "CANVAS_LIBELLE", type: "string" },
        { name: "QUERY_DONNE_BASE", type: "string" },
        { name: "VALEUR_AFFICHER", type: "string" },
        { name: "VALEUR_RETOUR", type: "string" },
        { name: "ID_FORMAT_DONNEE", type: "int" },
        { name: "LIBELLE_FORMAT_DONNEE", type: "string" },
      ],
      id: "ID_PARAM",
      localdata: [],
    };
    this.dataAdapter = new jqx.dataAdapter(this.source);
    this.columns = [
      {
        text: "ID Paramètre",
        datafield: "ID_PARAM",
        editable: false,
        cellsalign: "center",
        width: 100,
        align: "center",
      },
      {
        text: "Libellé",
        datafield: "LIBELLE_PARAM",
        editable: false,
        cellsalign: "center",
        width: 150,
        align: "center",
      }, {
        text: "is primary key",
        datafield: "IS_PRIMARY_KEY",
        editable: false,
        cellsalign: "center", columntype: "checkbox",
        filtertype: "checkedlist",

        width: 150,
        align: "center",
      },
      {
        text: "Requête Donnée de base",
        datafield: "QUERY_DONNE_BASE",
        editable: false,
        cellsalign: "center",
        align: "center",
        width: 200,
      },
      {
        text: "Valeur afficher",
        datafield: "VALEUR_AFFICHER",
        editable: false,
        cellsalign: "center",
        width: 150,
        align: "center",
      },
      {
        text: "Valeur retour",
        datafield: "VALEUR_RETOUR",
        editable: false,
        cellsalign: "center",
        width: 150,
        align: "center",
      },
      {
        text: "ID Format Données",
        datafield: "ID_FORMAT_DONNEE",
        width: 150,
        editable: false,
        cellsalign: "center",
        align: "center",
      }, {
        text: "Libelle format donnee",
        datafield: "LIBELLE_FORMAT_DONNEE",
        width: 150,
        editable: false,
        cellsalign: "center",
        align: "center",
      },
    ];
  }
  private fillGrid(ID_CANVAS: number): void {
    let pagesize: number = 20;
    if (this.Grid) {
      pagesize = this.Grid.getpaginginformation().pagesize;
    }

    this.source.localdata = [];
    let selectedcanvas = this.canvas.find(c => c.ID_CANVAS == ID_CANVAS);
    if (selectedcanvas.PARAMS)
      if (this.source.localdata.length <= selectedcanvas.PARAMS.length) {
        selectedcanvas.PARAMS.forEach(element => {
          this.source.localdata.push({
            ...element, LIBELLE_FORMAT_DONNEE: ((element.FORMAT_DONNEE) ? element.FORMAT_DONNEE.LIBELLE_FORMAT_DONNEE : "null")
          });
        });
        if (this.Grid)
          this.Grid.updatebounddata();
      }
  }
  onSelectModel(event: any) {
    this.source.localdata = [];
    this.Grid.updatebounddata();
    this.form.get('canvas').reset();
    this.canvasService.findAllBelongsTo(event.ID_MDM_MODEL).pipe(takeUntil(this.subscriptions))
      .subscribe(
        (value) => {
          this.canvas = value.data;
          this.canvas = value.data;
        },
        (error) => {
          this.alertBoxService.alert({
            icon: "error",
            title: error.statusText,
            text: error.error.message,
          });
        }
      );
  }

  onSelectCanvas(event: any) {
    this.fillGrid(event.ID_CANVAS);
  }
}
