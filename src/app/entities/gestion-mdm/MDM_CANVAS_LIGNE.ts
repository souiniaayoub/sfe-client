import { ParamLigneCanvas } from "./MDM_PARAM_LIGNE_CANVAS";
import { Canvas } from "./MDM_CANVAS";
export class CanvasLigne {
  ID_CANVAS_LIGNE: number;
  ID_CANVAS: number;
  SYNCHRO: boolean;
  ANNULER: boolean;
  ID_OP_SAISIE: number;
  DATETIME_SAISIE: Date;
  PARAM_LIGNE_CANVAS: ParamLigneCanvas[];
  CANVAS: Canvas;
}
