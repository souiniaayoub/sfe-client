import { Canvas } from "./MDM_CANVAS";
import { FormatDonnee } from "./MDM_FORMAT_DONNEE";
export class Param {
  ID_PARAM: number;
  LIBELLE_PARAM: string;
  QUERY_DONNE_BASE: string;
  VALEUR_AFFICHER: string;
  VALEUR_RETOUR: string;
  ID_FORMAT_DONNEE: number;
  ID_OP_SAISIE: number;
  DATETIME_SAISIE: Date;
  CANVAS: Canvas;
  IS_PRIMARY_KEY: boolean;
  FORMAT_DONNEE: FormatDonnee;
}
