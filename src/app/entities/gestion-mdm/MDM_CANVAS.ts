import { Param } from "./MDM_PARAM";
import { Model } from "./MDM_MODEL";
import { CanvasLigne } from "./MDM_CANVAS_LIGNE";
export class Canvas {
  ID_CANVAS: number;
  LIBELLE: string;
  ACTIF: boolean;
  ID_MDM_MODEL: number;
  ID_OP_SAISIE: number;
  DATETIME_SAISIE: Date;
  PARAMS: Param[];
  MODEL: Model;
  CANVAS_LIGNES: CanvasLigne[];
}
