import { CanvasLigne } from "./MDM_CANVAS_LIGNE";
import { Motif } from "./MDM_MOTIF";
import { Param } from "./MDM_PARAM";
export class ParamLigneCanvas {
  ID_PARAM_LIGNE_CANVAS: number;
  ID_CANVAS_LIGNE: number;
  ID_PARAM: number;
  CONTENU: string;
  VALID: boolean;
  ID_OP_SAISIE: number;
  DATETIME_SAISIE: Date;
  CANVAS_LIGNE: CanvasLigne;
  MOTIFS: Motif[];
  PARAM: Param;
}
