export class FormatDonnee {
  ID_FORMAT_DONNEE: number;
  LIBELLE_FORMAT_DONNEE: string;
  ID_OP_SAISIE: number;
  DATETIME_SAISIE: Date;
}
