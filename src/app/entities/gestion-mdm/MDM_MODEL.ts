import { Canvas } from "./MDM_CANVAS";
export class Model {
  ID_MDM_MODEL: number;
  LIBELLE: string;
  ACTIF: boolean;
  ID_OP_SAISIE: number;
  DATETIME_SAISIE: Date;
  CANVAS: Canvas[];
}
