import { ParamLigneCanvas } from "./MDM_PARAM_LIGNE_CANVAS";
export class Motif {
  ID_MOTIF: number;
  ID_PARAM_CONTENU: number;
  MOTIF: string;
  ID_OP_VERIFIER: number;
  DATETIME_VERIFIER: Date;
  PARAM_LIGNE_CANVAS: ParamLigneCanvas;
}
