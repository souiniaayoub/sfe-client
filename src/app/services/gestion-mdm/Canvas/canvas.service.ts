import { Injectable } from "@angular/core";
import { Canvas } from "../../../entities/gestion-mdm/MDM_CANVAS";
import { HttpHelperService } from "../../utils/http-helper.service";
@Injectable({
  providedIn: "root",
})
export class CanvasService {
  private className = "/api/gestionMDM/canvas";
  constructor(private httpMethode: HttpHelperService) { }
  findAll() {
    const path = "/findAll";

    return this.httpMethode.get(this.className + path);
  }
  /**
 * @param {number} ID_CANVAS
 */
  findOneIncludeParam(ID_CANVAS: number) {
    const path = "/findOneIncludeParam/" + ID_CANVAS;

    return this.httpMethode.get(this.className + path);
  }
  /**
 * @param {number} ID_CANVAS
 */
  HasPrimaryKey(ID_CANVAS: number) {
    const path = "/HasPrimaryKey/" + ID_CANVAS;

    return this.httpMethode.get(this.className + path);
  }
  /**
 * @param {number} ID_MDM_MODEL
 */
  findAllBelongsTo(ID_MDM_MODEL: number) {
    const path = "/findAllBelongsTo/" + ID_MDM_MODEL;

    return this.httpMethode.get(this.className + path);
  }
  /**
* @param {number} ID_MDM_MODEL
*/
  findAllActifBelongsTo(ID_MDM_MODEL: number) {
    const path = "/findAllActifBelongsTo/" + ID_MDM_MODEL;

    return this.httpMethode.get(this.className + path);
  }
  findAllIncludeParamModel() {
    const path = "/findAllIncludeParamModel";

    return this.httpMethode.get(this.className + path);
  }
  /**
   * @param {string} libelle
   */
  findHasLibelle(libelle: string) {
    const path = "/findHasLibelle/" + libelle;

    return this.httpMethode.get(this.className + path);
  }
  /**
   * @param {Canvas} entityObj
   */
  create(entityObj: Canvas) {
    const path = "/create";
    return this.httpMethode.post(this.className + path, entityObj);
  }
  /**
   * @param {Canvas} entityObj
   */
  createWithParams(entityObj: Canvas) {
    const path = "/createWithParams";
    return this.httpMethode.post(this.className + path, entityObj);
  }
  /**
  * @param {Canvas} entityObj
  */
  update(entityObj: Canvas) {
    const path = "/update";
    return this.httpMethode.put(this.className + path, entityObj);
  }
}
