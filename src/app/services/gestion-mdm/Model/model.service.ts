import { Injectable } from "@angular/core";
import { Model } from "../../../entities/gestion-mdm/MDM_MODEL";
import { HttpHelperService } from "../../utils/http-helper.service";
@Injectable({
  providedIn: "root",
})
export class ModelService {
  private className = "/api/gestionMDM/model";
  constructor(private httpMethode: HttpHelperService) { }
  findAll() {
    const path = "/findAll";

    return this.httpMethode.get(this.className + path);
  }
  findAllActif() {
    const path = "/findAllActif";

    return this.httpMethode.get(this.className + path);
  }
  /**
   * @param {string}  libelle
   */
  findHasLibelle(libelle: string) {
    const path = "/findHasLibelle/" + libelle;

    return this.httpMethode.get(this.className + path);
  } /**
   * @param {number}  ID_MDM_MODEL
   */
  findAllCanvasIncludeParams(ID_MDM_MODEL: string) {
    const path = "/findHasLibelle/" + ID_MDM_MODEL;

    return this.httpMethode.get(this.className + path);
  }
  // findAllIncludeParam() {
  //   const path = "/findAllIncludeParam";

  //   return this.httpMethode.get(this.className + path);
  // }
  findAllIncludeCanvas() {
    const path = "/findAllIncludeCanvas";

    return this.httpMethode.get(this.className + path);
  }
  /**
   * @param {Model} entityObj
   */
  create(entityObj: Model) {
    const path = "/create";
    return this.httpMethode.post(this.className + path, entityObj);
  }
  /**
   * @param {Model} entityObj
   */
  update(entityObj: Model) {
    const path = "/update";
    return this.httpMethode.put(this.className + path, entityObj);
  }
}
