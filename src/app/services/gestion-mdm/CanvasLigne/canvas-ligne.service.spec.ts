import { TestBed } from '@angular/core/testing';

import { CanvasLigneService } from './canvas-ligne.service';

describe('CanvasLigneService', () => {
  let service: CanvasLigneService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CanvasLigneService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
