
import { Injectable } from "@angular/core";
import { HttpHelperService } from "../../utils/http-helper.service";
import { ParamLigneCanvas } from '../../../entities/gestion-mdm/MDM_PARAM_LIGNE_CANVAS';
import { CanvasLigne } from '../../../entities/gestion-mdm/MDM_CANVAS_LIGNE';
@Injectable({
  providedIn: "root",
})
export class CanvasLigneService {
  private className = "/api/gestionMDM/canvas-ligne";
  constructor(private httpMethode: HttpHelperService) { }
  findAllIncludeParamLigneCanvas(ID_CANVAS: number) {
    const path = "/findAllIncludeParamLigneCanvas/" + ID_CANVAS;

    return this.httpMethode.get(this.className + path);
  }
  /**
* @param {string} Value
*/
  findAllByParamPK(Value: string) {
    const path = "/findAllByParamPK/" + Value;

    return this.httpMethode.get(this.className + path);
  }
  /**
* @param {number} ID_CANVAS_LIGNE
*/
  NonValidParams(ID_CANVAS_LIGNE: number) {
    const path = "/NonValidParams/" + ID_CANVAS_LIGNE;

    return this.httpMethode.get(this.className + path);
  }
  /**
 * @param entityObj
 */
  createWithParams(entityObj: CanvasLigne) {
    const path = "/createWithParams";
    return this.httpMethode.post(this.className + path, entityObj);
  }
  /**
* @param entityObj
*/
  create(entityObj: CanvasLigne) {
    const path = "/create";
    return this.httpMethode.post(this.className + path, entityObj);
  }
  /**
 * @param entityObj
 */
  update(entityObj: CanvasLigne) {
    const path = "/update";
    return this.httpMethode.put(this.className + path, entityObj);
  }
}
