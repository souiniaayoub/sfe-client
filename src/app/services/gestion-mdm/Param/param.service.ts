import { Injectable } from "@angular/core";
import { HttpHelperService } from "../../utils/http-helper.service";
@Injectable({
  providedIn: "root",
})
export class ParamService {
  private className = "/api/gestionMDM/param";
  constructor(private httpMethode: HttpHelperService) { }
  findAll() {
    const path = "/findAll";
    return this.httpMethode.get(this.className + path);
  }
  findAllIncludeCanvas() {
    const path = "/findAllIncludeCanvas";
    return this.httpMethode.get(this.className + path);
  }
  findQueryDonneeData(ID_Param: number) {
    const path = "/findQueryDonneeData/" + ID_Param;
    return this.httpMethode.get(this.className + path);
  }
  create(entityObj) {
    const path = "/create";
    return this.httpMethode.post(this.className + path, entityObj);
  }
  update(entityObj) {
    const path = "/update";
    return this.httpMethode.put(this.className + path, entityObj);
  }
}
