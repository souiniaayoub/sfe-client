import { Motif } from '../../../entities/gestion-mdm/MDM_MOTIF';

import { Injectable } from "@angular/core";
import { HttpHelperService } from "../../utils/http-helper.service";
@Injectable({
  providedIn: "root",
})
export class MotifService {
  private className = "/api/gestionMDM/motif";
  constructor(private httpMethode: HttpHelperService) { }
  /**
   * @param {Motif} entityObj
   */
  create(entityObj: Motif) {
    const path = "/create";
    return this.httpMethode.post(this.className + path, entityObj);
  }


}
