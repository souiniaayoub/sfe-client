import { TestBed } from '@angular/core/testing';

import { ParamLigneCanvasService } from './param-ligne-canvas.service';

describe('ParamLigneCanvasService', () => {
  let service: ParamLigneCanvasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ParamLigneCanvasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
