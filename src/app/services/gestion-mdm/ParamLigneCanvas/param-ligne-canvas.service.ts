
import { Injectable } from "@angular/core";
import { HttpHelperService } from "../../utils/http-helper.service";
import { ParamLigneCanvas } from '../../../entities/gestion-mdm/MDM_PARAM_LIGNE_CANVAS';
@Injectable({
  providedIn: "root",
})
export class ParamLigneCanvasService {
  private className = "/api/gestionMDM/param-ligne-canvas";
  constructor(private httpMethode: HttpHelperService) { }
  /**
   * @param entityObj
   */
  update(entityObj: ParamLigneCanvas) {
    const path = "/update";
    return this.httpMethode.put(this.className + path, entityObj);
  }
  create(entityObj: ParamLigneCanvas) {
    const path = "/create";
    return this.httpMethode.post(this.className + path, entityObj);
  }



}