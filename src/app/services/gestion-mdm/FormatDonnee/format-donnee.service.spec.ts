import { TestBed } from '@angular/core/testing';

import { FormatDonneeService } from './format-donnee.service';

describe('FormatDonneeService', () => {
  let service: FormatDonneeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FormatDonneeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
