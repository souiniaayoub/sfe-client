import { Injectable } from "@angular/core";
import { HttpHelperService } from "../../utils/http-helper.service";
@Injectable({
  providedIn: "root",
})
export class FormatDonneeService {
  private className = "/api/gestionMDM/format-donnee";
  constructor(private httpMethode: HttpHelperService) {}
  findAll() {
    const path = "/findAll";
    return this.httpMethode.get(this.className + path);
  }
}
